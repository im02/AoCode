VC播放声音函数PlaySound和sndPlaySound的用法 
一、PlaySound函数的声明为：

BOOL PlaySound(LPCSTR pszSound,HMODULE hmod,DWORD fdwSound);

参数说明：

pszSound：是指定了要播放声音的字符串,该参数可以是WAVE文件的名字,或是WAVE资源的名字,或是内存中声音数据的指针,或是在系统注册表WIN.INI中定义的系统事件声音.如果该参数为NULL则停止正在播放的声音.

hmod：是应用程序的实例句柄,当播放WAV资源时要用到该参数,否则它必须为NULL.

fdwSound： 是标志的组合，如下表所示。若成功则函数返回TRUE，否则返回FALSE。
二、播放标志以及含义：

SND_APPLICATION
用应用程序指定的关联来播放声音。

SND_ALIAS
pszSound参数指定了注册表或WIN.INI中的系统事件的别名。

SND_ALIAS_ID
pszSound参数指定了预定义的声音标识符。

SND_ASYNC
用异步方式播放声音，PlaySound函数在开始播放后立即返回。

SND_FILENAME
pszSound参数指定了WAVE文件名。

SND_LOOP
重复播放声音，必须与SND_ASYNC标志一块使用。

SND_MEMORY
播放载入到内存中的声音，此时pszSound是指向声音数据的指针。

SND_NODEFAULT
不播放缺省声音，若无此标志，则PlaySound在没找到声音时会播放缺省声音。

SND_NOSTOP
PlaySound不打断原来的声音播出并立即返回FALSE。

SND_NOWAIT
如果驱动程序正忙则函数就不播放声音并立即返回。
SND_PURGE
停止所有与调用任务有关的声音。若参数pszSound为NULL，就停止所有的声音，否则，停止pszSound指定的声音。
SND_RESOURCE
pszSound参数是WAVE资源的标识符，这时要用到hmod参数。
SND_SYNC
同步播放声音，在播放完后PlaySound函数才返回。

三、函数使用方法及代码：

注意：在使用函数前要加入：

#include "mmsystem.h"//导入声音头文件

#pragma comment(lib,"winmm.lib")//导入声音头文件库

１、直接播出声音文件：

PlaySound("c:\\win95\\media\\The Microsoft Sound.wav", NULL, SND_FILENAME | SND_ASYNC);
注意：参数中的路径使用两个连续的反斜杠转义代表一个反斜杠。

２、把声音文件加入到资源中，然后从资源中播放声音：

Visual C++支持WAVE型资源，用户在资源视图中单击鼠标右键并选择Import命令，然后在文件选择对话框中选择The Microsoft Sound.wav文件，则该文件就会被加入到WAVE资源中。假定声音资源的ID为IDR_STARTWIN，则下面的调用同样会输出启动声音：

PlaySound((LPCTSTR)IDR_STARTWIN, AfxGetInstanceHandle(), SND_RESOURCE | SND_ASYNC);

或：

PlaySound(MAKEINTRESOURCE(IDR_WAVE2),AfxGetResourceHandle(),SND_ASYNC|SND_RESOURCE|SND_NODEFAULT|SND_LOOP);//将声音文件写入到程序中
３、用PlaySound播放系统声音：

Windows启动的声音是由SystemStart定义的系统声音，因此可以用下面的方法播放启动声音：
PlaySound("SystemStart",NULL,SND_ALIAS|SND_ASYNC);
            

sndPlaySound函数的声明为：
BOOL sndPlaySound(LPCSTR lpszSound, UINT fuSound);

用法：

除了不能指定资源名字外，参数lpszSound与PlaySound的是一样的。参数fuSound是如何播放声音的标志，可以是SND_ASYNC、SND_LOOP、SND_MEMORY、SND_NODEFAULT、SND_NOSTOP和SND_SYNC的组合，这些标志的含义与PlaySound的一样。
可以看出，sndPlaySound不能直接播放声音资源。要用该函数播放WAVE文件，可按下面的方式调用：
sndPlaySound("MYSOUND.WAV",SND_ASYNC);
