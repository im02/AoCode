文档标题的更改方法
一·单文档接口（SDI）类型窗口标题的改变
　　 首先，我们来看Windows SDI类型窗口标题栏的结构形式。结合具体例子说明：用VC的AppWizard创建一个名为“SDITitle”的单文档类型的工程，创建完工程后，编译运行该工程，程序主窗口标题形式如下：
　　 文档标题 主窗口标题
　　 1·文档标题的改变
　　文档标题在是由工程中相应的文档类所控制的，因此我们可以利用SetTitle ()函数来改变文档标题。
例：改变CSDITitleDoc：：OnNewDocument() 函数为：
BOOL CSDITitleDoc::OnNewDocument()
　　 {
if (!CDocument::OnNewDocument())
return FALSE;
SetTitle (" 文档标题 " );
　　 return TRUE;
　　 }
　　运行程序，则每次选择选单中的“文件”的“新建”选项时，文档标题就变成 “文档标题- SDITitle”。
　　 2·主窗口标题的改变
　　主窗口的标题默认是工程的文件名，正如前面AppWazied生成的SDITitle工程执行后，标题栏的形式总为“（文档名）- SDITitle”。
　　 改变主窗口的标题有两种：
　　 第一种方法：利用ResourceView打开工程的资源，修改String Table中的IDR_MAINFRAME的值。将原来的“SDITitle\n\nTitle\n\n\nTitle.Document\nTitle Document”改为“标题改变示例\n\nTitle\n\n \nTitle. Document\nTitle Document”。这样改变后，主窗口标题栏的形式就为“((((-标题改变示例”。
　　 第二种方法：具体改变方法如下：
　　 改变CSDIMainFrame的成员函数PreCreateWindow()为：
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
　　 {
m_strTitle = " vc在线编程网 ";
return CFrameWnd::PreCreateWindow(cs);
　　 }
　　 这改变后，标题栏的形式就为“((((-vc在线编程网”。
　　在这两种改变方法中，第二种方法优先级高。也就是说，改变了m_strTitle的值后，IDR_MAINFRAME的值就不起作用。
　　 二·对多文档接口（MDI）类型窗口标题的改变
　　 MDI类型窗口标题中，主窗口标题和文档标题的次序，和SDI类型窗口中次序是不同的。结合具体例子说明，用VC的AppWizard创建一个名为 “MDITitle”的多文挡类型的工程。创建完工程后，编译运行该工程，主窗口标题形式如下：主窗口标题（当前打开的）文档标题
　　 1·文档标题的改变
　　 MDI类型窗口的文档标题也是由相应的文档类控制的。改变方法同SDI类型窗
口类似，要改变标题，只要在相应的文档类中利用SetTitle()函数来改变文档标
题。
　　 2·主窗口标题的改变
　　 MDI类型主窗口标题的改变方法同SDI类型窗口类似。改变主窗口标题也有两种方法，通过改变IDR_MAINFRAME或m_strTitle的值就可实现改变主窗口标题的目的。同样，改变了m_strTitle值后，IDR_MAINFRAME的值就不起作用。
　　 3·子窗口标题的改变
　　在默认情况下，MFC自动将子窗口标题设为该子窗口所打开的文档标题。但实际上，子窗口标题与该子窗口标题所打开的文档标题并不是同一回事。我们可做如下实验：
　　 (利用ResourceView打开资源，修改String Table中的IDR_MAINFRAME的值为“vc在线编程网”，使得主窗口标题变成“vc在线编程网”。
　　 (将BOOL CMDITitleDoc::OnNewDocument()函数改为：
　　 BOOL CMDITitleDoc::OnNewDocument()
　　 {
if (!CDocument::OnNewDocument())
return FALSE;
static int count=0;
char message[10];
wsprintf (message,"%s%d","文档",count);
SetTitle (message);
count++;
return TRUE;
　　 }
　　 设置文档标题为“文档0”、“文档1”... 。
　　 (r)将CChildFrame ：：PreCreateWindow() 函数改为：
BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
　　 {
cs.style &=~ (LONG) FWS_ADDTOTITLE;
return CMDIChildWnd::PreCreateWindow(cs);
　　 }
改变窗口类型，这步工作必须做。常量FWS_ADDTOTITLE是在文件\ msdev \ mfc \include \afxwin.h中定义。
　　 (在CMDITitleView类中增加OnInitialUpdate () 函数：
void CMDITitleView::OnInitialUpdate()
　　 {
CView::OnInitialUpdate();
GetParent()->SetWindowText(GetDocument()->GetTitle()+"-vc窗口标题示例");
　　 }这样，子窗口的标题栏的形式就为“文档名(- vc窗口标题示例”。

　　修改后的程序运行效果如下：上面例子也可做修改MDI类型窗口的各种标题参考。