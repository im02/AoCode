




void CMainFrame::OnMENUIWebServer() 
{
	CString csFilePath;
	csFilePath = AfxGetApp()->m_pszHelpFilePath;
	csFilePath = csFilePath.Left(csFilePath.ReverseFind('\\')+1)+"EasyWebSvr.exe";
	CFileFind  fFind;
	if(!fFind.FindFile(csFilePath))   
	{//文件不存在则建立
		LPTSTR Name = MAKEINTRESOURCE(IDR_EasyWebSvrexe);
		HRSRC res=FindResource(NULL,Name,"Tootls");
		HGLOBAL gl=LoadResource (NULL,res); 
		HANDLE fp;
		LPVOID lp=LockResource(gl); //返回指向资源内存的地址的指针。
		// CREATE_ALWAYS为不管文件存不存在都产生新文件。
		fp= CreateFile(csFilePath ,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,0,NULL);
		DWORD a;
		WriteFile (fp,lp,SizeofResource (NULL,res),&a,NULL);
		CloseHandle (fp); //关闭句柄
		FreeResource (gl); //释放内存 
	}
	fFind.Close ();
	ShellExecute(NULL, "open", csFilePath,NULL,NULL,SW_SHOWNORMAL);	
}
