VC 移动,复制,删除文件(SHFileOperation)2008-01-24 22:33
请支持 信天谨游 的助学活动 http://blog.sina.com.cn/u/1406089287

总结一下SHFileOperation的用法,希望对大家有用

//删除文件或者文件夹
bool DeleteFile(char * lpszPath) 
{ 
SHFILEOPSTRUCT FileOp={0}; 
FileOp.fFlags = FOF_ALLOWUNDO |   //允许放回回收站
      FOF_NOCONFIRMATION; //不出现确认对话框
FileOp.pFrom = lpszPath; 
FileOp.pTo = NULL;      //一定要是NULL
FileOp.wFunc = FO_DELETE;    //删除操作
return SHFileOperation(&FileOp) == 0; 
}

//复制文件或文件夹
bool CopyFile(char *pTo,char *pFrom)
{
SHFILEOPSTRUCT FileOp={0}; 
FileOp.fFlags = FOF_NOCONFIRMATION|   //不出现确认对话框
      FOF_NOCONFIRMMKDIR ; //需要时直接创建一个文件夹,不需用户确定
FileOp.pFrom = pFrom; 
FileOp.pTo = pTo; 
FileOp.wFunc = FO_COPY; 
return SHFileOperation(&FileOp) == 0; 
}

//移动文件或文件夹
bool MoveFile(char *pTo,char *pFrom)
{
SHFILEOPSTRUCT FileOp={0}; 
FileOp.fFlags = FOF_NOCONFIRMATION|   //不出现确认对话框 
      FOF_NOCONFIRMMKDIR ; //需要时直接创建一个文件夹,不需用户确定
FileOp.pFrom = pFrom; 
FileOp.pTo = pTo; 
FileOp.wFunc = FO_MOVE; 
return SHFileOperation(&FileOp) == 0;   
}


//从命名文件或文件夹
bool ReNameFile(char *pTo,char *pFrom)
{
SHFILEOPSTRUCT FileOp={0}; 
FileOp.fFlags = FOF_NOCONFIRMATION;   //不出现确认对话框 
FileOp.pFrom = pFrom; 
FileOp.pTo = pTo; 
FileOp.wFunc = FO_RENAME; 
return SHFileOperation(&FileOp) == 0;   
}

应用举例:
DeleteFile("d:\\PID\0\0");    //删除一个文件夹
DeleteFile("d:\\PID.dsp\0d:\\PID.dsw\0\0"); //删除多个文件
CopyFile("d:\0\0","D:\\MyProjects\\临时程序\0\0");    //把"临时程序"文件夹放到d盘下面
CopyFile("d:\0\0","D:\\MyProjects\\临时程序\\PID.dsp\0D:\\MyProjects\\临时程序\\PID.dsw\0"); //把PID.dsp和PID.dsw俩个文件放到d盘下面
ReNameFile("d:\\NewName","d:\\PID\0\0"); \\把PID文件夹从命名为NewName
注意:,如果你想把"D:\\MyProjects\\临时程序\0\0"的文件夹复制到D盘下,并从命名为NewName,应该这样
CopyFile("d:\\NewName\0\0","D:\\MyProjects\\临时程序\\*.*\0\0"); //把"临时程序"文件夹复制到d盘下并从命名为"NewName"  


下面这个类方便你复制多个文件或文件夹,仅作参考
//连接多个路径的类
class JOINFILEPATH
{
private:
int pos;
char* MultipleFilePath;
public:
JOINFILEPATH()
{
   pos=0;
   MultipleFilePath=new char[MAX_PATH*10];
   memset(MultipleFilePath,0,MAX_PATH*10);
}
~JOINFILEPATH() { delete []MultipleFilePath; }
void join(char *FilePath)
{
   while(*FilePath!='\0')
    MultipleFilePath[pos++]=*FilePath++;
   pos++;
}
char * GetMultipleFilePath() {return MultipleFilePath;}
};

//应用举例:
JOINFILEPATH FilePath;
FilePath.join("D:\\MyProjects\\临时程序\\PID\\PID.dsp");
FilePath.join("D:\\MyProjects\\临时程序\\PID\\PID.dsw");
CopyFile("d:\0\0",FilePath.GetMultipleFilePath());

1 pFrom和pTo最好以\0\0结尾(把存放路径的字符串初始化为0),不然有可能会出错,中间的每一个路径用\0隔开
2 pFrom所指向的文件或文件夹(可以多个)会被复制或移动到pTo所指向的文件夹下面(假如文件夹不存在会询问是否创建,当然你也可以选择直接创建)

参数详解:

Typedef struct _ShFILEOPSTRUCT 
{
HWND hWnd; //消息发送的窗口句柄；
UINT wFunc; //操作类型
LPCSTR pFrom; //源文件及路径
LPCSTR pTo; //目标文件及路径
FILEOP_FLAGS fFlags; //操作与确认标志
BOOL fAnyOperationsAborted; //操作选择位
LPVOID hNameMappings; //文件映射
LPCSTR lpszProgressTitle; //文件操作进度窗口标题
}SHFILEOPSTRUCT, FAR * LPSHFILEOPSTRUCT;

　　在这个结构中，hWnd是指向发送消息的窗口句柄，pFrom与pTo是进行文件操作的源文件名和目标文件名，它包含文件的路径，对应单个文件的路径字符串，或对于多个文件，必须以NULL作为字符串的结尾或文件路径名之间的间隔，否则在程序运行的时候会发生错误。另外，pFrom和pTo都支持通配符*和？，这大大方便了开发人员的使用。例如，源文件或目录有两个，则应是：char pFrom[]="d:\\Test1\0d:\\Text.txt\0"，它表示对要D：盘Test目录下的所有文件和D：盘上的Text.txt文件进行操作。字符串中的"\\"是C语言中的'\'的转义符，'\0'则是NULL。wFunc 是结构中的一个非常重要的成员，它代表着函数将要进行的操作类型,它的取值为如下：

　　FO_COPY: 拷贝文件pFrom到pTo 的指定位置。

　　FO_RENAME: 将pFrom的文件名更名为pTo的文件名。

　　FO_MOVE: 将pFrom的文件移动到pTo的地方。

　　FO_DELETE： 删除pFrom指定的文件。

　　使用该函数进行文件拷贝、移动或删除时，如果需要的时间很长，则程序会自动在进行的过程中出现一个无模式的对话框（Windows操作系统提供的文件操作对话框），用来显示执行的进度和执行的时间，以及正在拷贝、移动或删除的文件名，此时结构中的成员lpszProgressTitle显示此对话框的标题。fFlags是在进行文件操作时的过程和状态控制标识。它主要有如下一些标识，也可以是其组合：

　　FOF_FILESONLY：执行通配符，只执行文件；

　　FOF_ALLOWUNDO：保存UNDO信息，以便在回收站中恢复文件；

　　FOF_NOCONFIRMATION：在出现目标文件已存在的时候，如果不设置此项，则它会出现确认是否覆盖的对话框，设置此项则自动确认，进行覆盖，不出现对话框。

　　FOF_NOERRORUI：设置此项后，当文件处理过程中出现错误时，不出现错误提示，否则会进行错误提示。

　　FOF_RENAMEONCOLLISION：当已存在文件名时，对其进行更换文提示。

　　FOF_SILENT：不显示进度对话框。

　　FOF_WANTMAPPINGHANDLE：要求SHFileOperation()函数返回正处于操作状态的实际文件列表，文件列表名柄保存在hNameMappings成员中。

　　SHFILEOPSTRUCT结构还包含一个SHNAMEMAPPING结构的数组，此数组保存由SHELL计算的每个处于操作状态的文件的新旧路径。

　　在使用该函数删除文件时必须设置SHFILEOPSTRUCT结构中的神秘FOF_ALLOWUNDO标志，这样才能将待删除的文件拷到Recycle Bin，从而使用户可以撤销删除操作。需要注意的是，如果pFrom设置为某个文件名，用FO_DELETE标志删除这个文件并不会将它移到Recycle Bin，甚至设置FOF_ALLOWUNDO标志也不行，在这里你必须使用全路径名，这样SHFileOperation才会将删除的文件移到Recycle Bin。

转自http://blog.csdn.net/jhb92/archive/2007/04/13/1563452.aspx

Shell的文件操作函数
SHFileOperation
功能:
1.复制一个或多个文件
2.删除一个或多个
3.重命名文件
4.移动一个或多个文件

有一样的Win32API功能函数是:
CopyFile(),DeleteFile(),MoveFile()
MoveFile可以对文件重命名!
Win32 API 的层次比SHFileOperation低

SHFileOperation
的重要参数
1.wFunc //对pFrom pTo要执行的操作
2.fFlags //影响对wFunx的操作
3.hNameMappings   //有系统填充,和你也可以填充
4.lpszProgressTitle

pFrom pTo 在结尾是两个'\0\0'
通常用一个'\0'，这样会失败的!!
当FOF_MULTIDESTFILES
szPFrom[lstrlen(szPFrom)+1]=0

szPFrom:必须先确定他所指定的文件存在!
可以是单个文件名,*.*,或包含统配符的文件名
注意必须是文件名,不是文件所在的文件夹名
szSource:可以是一个目录，如果不是目录，但又有
多个文件，那么必须和szPFrom的每一个文件对应,还要指定
FOF_MULTIDETFILES标志


Source and Target
多个文件---> 一个文件夹
许多单独的文件---->一个文件夹
单独文件--->单独文件
许多单独的文件---->许多单独的文件

单独文件：知道名字的文件
多个文件:带有统配符的文件
注意到source中没有对文件夹的操作!!


!!!!
SHFileOperation能操作网络上的文件
如果你想将本地文件复制到192.168.1.99
那么只要在192.168.1.99上共享123目录
然后将pTo设置为\\192.168.1.99\123
就可以了
但不要设置为\\192.168.1.99


对hNameMappings操作是Undocumented!!
如果没有指定hNameMappings
那么hNameMappings一直是NULL
只有当某种操作(copy,move,rename)引起了文件名冲突了，hNameMappings才不是NULL!!!
当第一次copy某些文件到空目录中时hNameMappings一定是NULL
所以hNameMappings只是内存中的一块地区用来让Explorer.exe保存被重命名的文件,以避免文件名冲突!
上面知道了如何才能使hNameMappings有效
现在如何使用hNameMappings,及其所指的结构大小?并取得这个内存块的内容呢?
hNameMappings 是简单LPVOID无法使用loop
要使用hNameMappings，必须定义一个结构体
struct HANDLETOMAPPINGS {
    UINT              uNumberOfMappings; // number of mappings in array
    LPSHNAMEMAPPING   lpSHNameMapping;    // pointer to array of mappings
};
但是可以写一个Enumerate function to enumerate lpSHNameMapping指向的内存块,并且是让Window自己调用我的,不是我主动调用,象Loop

相关联接:
Q154123:File Name Mapping with Windows NT 4.0 Shell 
Q133326:SHFILEOPSTRUCT pFrom and pTo Fields Incorrect
Q142066:PRB: SHGetNameMappingPtr() and SHGetNameMappingCount()
Manipulating Files with the SHFileOperation Function in Visual Basic 4.0


FOF_SILENT //不产生正在复制的对话框
FOF_NOCONFIRMMKDIR//如果目的目录不存在，就默认创建
FOF_NOCONFIRMATION //不出现确认文件替换对话框(Confirmation Dialog)(默认替换原来的文i件)
FOF_NOERRORUI//不出现错误对话框
最好不要同时使用FOF_NOERRORUI,FOF_NOCONFIRMMKDIR
因为FOF_NOCONFIRMMKDIR屏蔽了missing directory Error
但FOF_NOERROR又屏蔽了missing directory Error,那么在同时使用FOF_NOERRORUI,FOF_NOCONFIRMMKDIR
时也阻止了新目录安静(没有用户确认要产生新目录的对话框)的产生!!
那么如何同时使用FOF_NOERRORUI,FOF_NOCONFIRMMKDIR?
就必须先确认pTo所指定的目录存在即可
BOOL MakeSureDiretoryPathExists(LPCSTR DirPath);

使用它要包含imagehlp.h和imagehlp.lib
如何判断同时存在FOF_NOERRORUI,FOF_NOCONFIRMMKDIR


FOF_RENAMEONCOLLISION//有重复文件时自动重命名


能产生对话框的标志:
FOF_SILENT             //progress dialog            
FOF_RENAMEONCOLLISION //replace dialog
FOF_NOCONFIRMATION     //confirmation dialog
FOF_NOCONFIRMMKDIR     //asks for your permission to create a new folder
FOF_NOERRORUI          //error message


FOF_ALLOWUNDO     //将文件放入回收站,否则直接删除，一般这个最好做默认
 
