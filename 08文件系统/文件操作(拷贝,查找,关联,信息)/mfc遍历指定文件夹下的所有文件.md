
### 源码名称：mfc遍历指定文件夹下的所有文件
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
mfc遍历指定文件夹下的所有文件
```
#### 下载：无
#### 使用方法：


``` c++
MFC下遍历文件夹下的所有文件，借助于CString类和CFileFind类的便捷，代码如下：

只有一层文件结构

很多时候我们要处理的文件只在一个特定的文件夹下，且该路径下除了待处理的文件之外没有其他文件夹，这时情况比较简单，不需要迭代处理，直接按照下面的操作即可：

    CString filepath = _T("/path/to/folder/"); 
    CString filename = _T("");
    CString fullname = _T("");

    CFileFind find;
    BOOL IsFind = find.FindFile(filepath + _T("/*.*"));

    while (IsFind)
    {
        IsFind = find.FindNextFile();
        if (find.IsDots())
        {
            continue;
        ｝
        else
        {
            filename = find.GetFileName();
            fullname = filepath + filename;
            cout << fullname << endl;
        ｝
    ｝

            
多层文件结构

有时候我们处理的文件有多个文件结构，也就是说文件夹下面还有文件夹，这时候需要采用递归的方式遍历。举个例子，比如我们要处理一批后缀名为.bmp的文件,且这些文件在一个根目录下面的很多子目录下，那么我们可以这样处理：

void BroseAllFiles(CString filepath)
{
    //检测路径是否正确并添加必要信息
    if (filepath == _T(""))
    {
        return;
    }
    else 
    {
        if (filepath.Right(1) != _T(""))
        {
            filepath += _T("\\");
        }
        filepath += _T("*.*");
    }

    //递归枚举文件夹下的内容
    CFileFind find;
    CString strpath;
    CString str_fileName;
    CString fullname;
    BOOL IsFind = find.FindFile(filepath);

    while (IsFind)
    {
        IsFind = find.FindNextFile();
        strpath = find.GetFilePath();

        if (find.IsDirectory() && !find.IsDots())
        {
            BroseAllFiles(strpath);
        }
        else if (!find.IsDierctory() && !find.IsDots())
        {
            str_fileName = find.GetFileName();
            if (str_fileName.Right(3) == _T("bmp")) //如果后缀是bmp文件才处理
            {
                fullname = strpath + str_fileName;
                cout << fullname << endl;
            }
        }
        else 
        {
            continue;
        }
    }
}


```
