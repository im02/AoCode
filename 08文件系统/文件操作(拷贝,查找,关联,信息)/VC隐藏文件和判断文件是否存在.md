一：隐藏文件。 
    1.WinExec函数。 
      该函数执行一个cmd命令，如修改 
      C:\\Documents and Settings\\eMLab\\Application Data\\test.txt 
      文件属性为隐藏可以： 
      CString strFileName = 
       "C:\\Documents and Settings\\eMLab\\Application Data\\test.txt"; 
      CString strCmd = "attrib +h" + strFileName; 
      WinExec(strCmd,0); 
      attrib修改文件属性，+h表示给文件加上隐藏属性。 
    2.SetFileAttributes函数 
      原型：BOOL SetFileAttributes(LPCTSTR   lpFileName,   //file name 
                                   WORD   dwFileAttributes //file attribute 
                                  );     
      如： 
      SetFileAttributes(strFileName,FILE_ATTRIBUTE_HIDDEN); 
　　　FILE_ATTRIBUTE_HIDDEN就表示隐藏属性。　 
    3.CFile和CFileStatus类 
      CFile的静态函数GetStatus可以读取文件状态 
      CFile的静态函数SetStatus可以修改文件状态 
      如： 
　　　FileStatus fs;   
      CFile::GetStatus(strFileName,fs);   
      fs.m_attribute = CFile::hidden;          //set hidden attribute 
      CFile::SetStatus(strFileName,fs);　 
二：判断文件是否存在。 
　　1.access函数，在io.h中。 
　　　原型：int access(const char *filename, int amode); 
      参数amode(好象有5种模式) 
      0:检查文件是否存在         
      1:检查文件是否可运行         
      2:检查文件是否可写访问     
      4:检查文件是否可读访问   
      还有一种，由于MSDN突然坏了，暂时保留着 
      if ( access(file,0) ) 
      { 
      //文件不存在 
      } 
    2.CFile和CFileStatus类 
      CFile的静态函数GetStatus如果返回FALSE表示文件不存在 
      CFileStatus fs;   
      if ( !CFile::GetStatus(strFileName,fs) ) 
      { 
       //文件不存在 
      } 

     3.CFileFind类 
       直接使用该类的成员函数FindFile进行判断 
       CFileFind ff;   
       if ( !ff.FindFile(strFileName) )   
       { 
       //文件不存在   
       } 
       ff.Close(); 
