方法一：
////
	WIN32_FIND_DATA fileInfo;
	HANDLE hFind;
	DWORD fileSize;
	const char *pTo = csToPath, *pFrom = csFrom;
	hFind = FindFirstFile(pTo ,&fileInfo);
	if(hFind != INVALID_HANDLE_VALUE)
		fileSize = fileInfo.nFileSizeLow;
	TRACE("====pTo = %d    ", fileSize);

	hFind = FindFirstFile(pFrom ,&fileInfo);
	if(hFind != INVALID_HANDLE_VALUE)
		fileSize = fileInfo.nFileSizeLow;
	TRACE("pFrom = %d ====   \n", fileSize);
	FindClose(hFind);
//*/////////////////////////////////////////////////////////////////////////

方法二：


#include <sys/types.h>
#include <sys/stat.h>




	struct stat buf;
	stat(pTo, &buf);
	fileSize = (unsigned long)buf.st_size;
	TRACE("====pTo = %d    ", fileSize);
	stat(pFrom, &buf);
	fileSize = (unsigned long)buf.st_size;
	TRACE("pFrom = %d ====   \n", fileSize);

	/////////////////////////////////////////////////////////////////////////*/
