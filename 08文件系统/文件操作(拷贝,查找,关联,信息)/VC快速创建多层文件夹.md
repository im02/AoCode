 VC快速创建多层文件夹！
分类： VC++ 2011-09-26 17:27 349人阅读 评论(0) 收藏 举报
BOOL CreateDirectory(
  LPCTSTR lpPathName,
  LPSECURITY_ATTRIBUTES lpSecurityAttributes
);
这个是大多数用户都知道的，但是这个有缺点，只能是一层一层的创建。。。很是麻烦！！！
 
下面给大家介绍一个函数:
BOOL MakeSureDirectoryPathExists(
  PCSTR DirPath
);
Client	Requires
 Windows XP or Windows 2000 Professional.
Server	Requires Windows Server 2003 or Windows 2000 Server.
Redistributable	Requires
 DbgHelp.dll on Windows NT 4.0 and Windows Me/98/95.
Header	
Declared in Dbghelp.h.

Library	
Link
 to Dbghelp.lib.

DLL	Requires Dbghelp.dll.
 

 

这个是MSDN上说的需要 Dbghelp.h  但是这样不管你怎么实验，都是不成功！

把头文件改成如下就可以正确使用了

#include "imagehlp.h"
#pragma comment(lib,"imagehlp.lib")

 

MakeSureDirectoryPathExists("F:\\DVR\\123\\123\\dfd\\asdf\\a\\adf\\")；

1.文件路径完全不存在。

2.文件路径存在一部分。

这些都是完全可以实现！！

 