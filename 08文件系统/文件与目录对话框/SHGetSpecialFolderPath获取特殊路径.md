SHGetSpecialFolderPath获取特殊路径 收藏 
VB格式：

Option Explicit Declare Function SHGetSpecialFolderPath Lib "shell32.dll" Alias "SHGetSpecialFolderPathA" (ByVal hwnd As Long, ByVal pszPath As String, ByVal csidl As Long, ByVal fCreate As Long) As Long
Const MAX_PATH As Long = 260
Const CSIDL_PERSONAL As Long = 5

Sub Main()
    Dim sPath As String
    sPath = Space(MAX_PATH) & Chr(0)     

    SHGetSpecialFolderPath 0, sPath, CSIDL_PERSONAL, 0
    Debug.Print sPath
End Sub
注意：Alias "SHGetSpecialFolderPathA" 不能省略

VC格式：

BOOL SHGetSpecialFolderPath (

         HWND hwndOwner,

         LPTSTR lpszPath,

         int nFolder,

         BOOL fCreate

);

hwndOwner ：用GetDesktopWindow API取一个窗口句柄。

lpszPath：返回的路径

nFolder：标识代号

fCreate：true ：如果文件夹不存在则创建，false：不创建



	TCHAR   szPath[MAX_PATH]; 
	int   iRet; 
	memset(szPath,   0,   sizeof(szPath)); 
	iRet   =   SHGetSpecialFolderPath(NULL,   szPath,   CSIDL_STARTMENU,   0); 
	if(TRUE   ==   iRet) 
	{ 
	} 


其他：

lpszPath和nFolder对应关系：

nFolder lpszPath 
0 C:\Documents and Settings\当前用户\桌面 
2 C:\Documents and Settings\当前用户\「开始」菜单\程序 
5 C:\Documents and Settings\当前用户\My Documents 
6 C:\Documents and Settings\当前用户\Favorites 
7 C:\Documents and Settings\当前用户\「开始」菜单\程序\启动 
8 C:\Documents and Settings\当前用户\Recent 
9 C:\Documents and Settings\当前用户\SendTo 
11 C:\Documents and Settings\当前用户\「开始」菜单 
13 C:\Documents and Settings\当前用户\My Documents\My Music 
14 C:\Documents and Settings\当前用户\My Documents\My Videos 
16 C:\Documents and Settings\当前用户\桌面 
19 C:\Documents and Settings\当前用户\NetHood 
20 C:\WINDOWS\Fonts 
21 C:\Documents and Settings\当前用户\Templates 
22 C:\Documents and Settings\All Users\「开始」菜单 
23 C:\Documents and Settings\All Users\「开始」菜单\程序 
24 C:\Documents and Settings\All Users\「开始」菜单\程序\启动 
25 C:\Documents and Settings\All Users\桌面 
26 C:\Documents and Settings\当前用户\Application Data 
27 C:\Documents and Settings\当前用户\PrintHood 
28 C:\Documents and Settings\当前用户\Local Settings\Application Data 
31 C:\Documents and Settings\All Users\Favorites 
32 C:\Documents and Settings\当前用户\Local Settings\Temporary Internet Files 
33 C:\Documents and Settings\当前用户\Cookies 
34 C:\Documents and Settings\当前用户\Local Settings\History 
35 C:\Documents and Settings\All Users\Application Data 
36 C:\WINDOWS 
37 C:\WINDOWS\system32 
38 C:\Program Files
39 C:\Documents and Settings\当前用户\My Documents\My Pictures 
40 C:\Documents and Settings\当前用户 
43 C:\Program Files\Common Files 
45 C:\Documents and Settings\All Users\Templates 
46 C:\Documents and Settings\All Users\Documents 
47 C:\Documents and Settings\All Users\「开始」菜单\程序\管理工具 
48 C:\Documents and Settings\当前用户\「开始」菜单\程序\管理工具 
53 C:\Documents and Settings\All Users\Documents\My Music 
54 C:\Documents and Settings\All Users\Documents\My Pictures 
55 C:\Documents and Settings\All Users\Documents\My Videos 
56 C:\WINDOWS\resources 
59 C:\Documents and Settings\当前用户\Local Settings\Application Data\Microsoft\CD Burning
