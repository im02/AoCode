SHFileOperation()函数主要对文件夹有四种操作：复制，删除，移动，重命名。
挤时间对这个函数进行了利用了一下。写了四个函数。可以很好的对文件夹进行操作。
/////////////////////////////////////
//函数名:DeleteFolder
//输入参数：LpszPath 要删除的路径指针
//作用：删除指定文件夹以及里面的文件
//
/////////////////////////////////////
BOOL DeleteFolder(LPCTSTR lpszPath)
{
int nLength = strlen(lpszPath);
char *NewPath = new char[nLength+2];
strcpy(NewPath,lpszPath);
NewPath[nLength] = '\0';
NewPath[nLength+1] = '\0';
SHFILEOPSTRUCT FileOp;
ZeroMemory((void*)&FileOp,sizeof(SHFILEOPSTRUCT));
FileOp.fFlags = FOF_NOCONFIRMATION;
FileOp.hNameMappings = NULL;
FileOp.hwnd = NULL;
FileOp.lpszProgressTitle = NULL;
FileOp.pFrom = NewPath;
FileOp.pTo = NULL;
FileOp.wFunc = FO_DELETE;
return SHFileOperation(&FileOp) == 0;
}
/////////////////////////////////////
//函数名：CopyFolder
//参数：lpszFromPath 源文件夹的路径 。 lpszToPath 目的文件夹的路径
//作用：拷贝文件夹及其文件夹中的所有内容
//
//////////////////////////////////////
BOOL CopyFolder(LPCTSTR lpszFromPath,LPCTSTR lpszToPath)
{
	TCHAR lpszFromPath[MAX_PATH]; 
int nLengthFrm = strlen(lpszFromPath);
char *NewPathFrm = new char[nLengthFrm+2];
strcpy(NewPathFrm,lpszFromPath);
NewPathFrm[nLengthFrm] = '\0';
NewPathFrm[nLengthFrm+1] = '\0';
SHFILEOPSTRUCT FileOp;
ZeroMemory((void*)&FileOp,sizeof(SHFILEOPSTRUCT));
FileOp.fFlags = FOF_NOCONFIRMATION ;
FileOp.hNameMappings = NULL;
FileOp.hwnd = NULL;
FileOp.lpszProgressTitle = NULL;
FileOp.pFrom = NewPathFrm;
FileOp.pTo = lpszToPath;
FileOp.wFunc = FO_COPY;
return SHFileOperation(&FileOp) == 0;
}
/////////////////////////////////////
//函数名：MoveFolder
//参数：lpszFromPath 源文件夹路径 。lpszToPath 目的文件夹路径
//作用：移动原文件夹及其中文件都指定的路径下
//
/////////////////////////////////////
BOOL MoveFolder(LPCTSTR lpszFromPath,LPCTSTR lpszToPath)
{
int nLengthFrm = strlen(lpszFromPath);
char *NewPathFrm = new char[nLengthFrm+2];
strcpy(NewPathFrm,lpszFromPath);
NewPathFrm[nLengthFrm] = '\0';
NewPathFrm[nLengthFrm+1] = '\0';
SHFILEOPSTRUCT FileOp;
ZeroMemory((void*)&FileOp,sizeof(SHFILEOPSTRUCT));
FileOp.fFlags = FOF_NOCONFIRMATION ;
FileOp.hNameMappings = NULL;
FileOp.hwnd = NULL;
FileOp.lpszProgressTitle = NULL;
FileOp.pFrom = NewPathFrm;
FileOp.pTo = lpszToPath;
FileOp.wFunc = FO_MOVE;
return SHFileOperation(&FileOp) == 0;
}
/////////////////////////////////////
//ReNameFolder
//参数：lpszFromPath 源文件夹路径 。lpszToPath 目的文件夹路径
//作用：修改原文件夹的名字。
//
/////////////////////////////////////
BOOL ReNameFolder(LPCTSTR lpszFromPath,LPCTSTR lpszToPath)
{
int nLengthFrm = strlen(lpszFromPath);
char *NewPathFrm = new char[nLengthFrm+2];
strcpy(NewPathFrm,lpszFromPath);
NewPathFrm[nLengthFrm] = '\0';
NewPathFrm[nLengthFrm+1] = '\0';
SHFILEOPSTRUCT FileOp;
ZeroMemory((void*)&FileOp,sizeof(SHFILEOPSTRUCT));
FileOp.fFlags = FOF_NOCONFIRMATION ;
FileOp.hNameMappings = NULL;
FileOp.hwnd = NULL;
FileOp.lpszProgressTitle = NULL;
FileOp.pFrom = NewPathFrm;
FileOp.pTo = lpszToPath;
FileOp.wFunc = FO_RENAME;
return SHFileOperation(&FileOp) == 0;
}
这四个函数在VC6.0下通过测试了一下，效果还是有的。不过感觉，FO_RENAME和FO_MOVE有点相似，而且是作用十分相似。

vbCritical Or vbOKOnly Else If fileop.fAnyOperationsAborted <> 0 Then MsgBox "Operation Failed", vbCritical Or vbOKOnly End If End If End Sub
SHFileOperation 
函数功能描述:文件操作，与 Shell 的动作相同. 
函数原型： 
#include<shellapi.h> 
WINSHELLAPI int WINAPI SHFileOperation(LPSHFILEOPSTRUCT lpFileOp); 
参数： 
typedef struct _SHFILEOPSTRUCT 
{ 
HWND hwnd; //父窗口句柄 
UINT wFunc; //要执行的动作 
LPCTSTR pFrom; //源文件路径，可以是多个文件 
LPCTSTR pTo; //目标路径，可以是路径或文件名 
FILEOP_FLAGS fFlags; //标志，附加选项 
BOOL fAnyOperationsAborted; //是否可被中断 
LPVOID hNameMappings; //文件映射名字，可在其它 Shell 函数中使用 
LPCTSTR lpszProgressTitle; // 只在 FOF_SIMPLEPROGRESS 时，指定对话框的标题。 
} SHFILEOPSTRUCT, FAR *LPSHFILEOPSTRUCT; 
=================== 
vb.net 
Public Structure SHFILEOPSTRUCT 
Dim hwnd As IntPtr 
Dim wFunc As Integer 
Dim pFrom As String 
Dim pTo As String 
Dim fFlags As Short 
Dim fAnyOperationsAborted As Integer 
Dim hNameMappings As IntPtr 
Dim lpszProgressTitle As String 
End Structure 
Public Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (ByRef lpFileOp As SHFILEOPSTRUCT) As Integer 
====================== 
vb: 
Type SHFILEOPSTRUCT 
hWnd As Long 
wFunc As Long 
pFrom As String '必须用 pFrom & vbNullChar & vbNullChar 
pTo As String '同pFrom 
fFlags As Integer 
fAnyOperationsAborted As Boolean 
hNameMappings As Long 
lpszProgressTitle As String 
End Type 
Public Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long 
======================= 
wFunc 可以为： 
/FO_MOVE 0x0001 移动文件 
FO_COPY 0x0002 复制文件 
FO_DELETE 0x0003 删除文件，只使用 pFrom 
FO_RENAME 0x0004 文件重命名 
fFlags可以为： 
FOF_MULTIDESTFILES 0x0001 //pTo 指定了多个目标文件，而不是单个目录 
FOF_CONFIRMMOUSE 0x0002 
FOF_SILENT 0x00044 // 不显示一个进度对话框 
FOF_RENAMEONCOLLISION 0x0008 // 碰到有抵触的名字时，自动分配前缀 
FOF_NOCONFIRMATION 0x0010 // 不对用户显示提示 
FOF_WANTMAPPINGHANDLE 0x0020 // 填充 hNameMappings 字段，必须使用 SHFreeNameMappings 释放 
FOF_ALLOWUNDO 0x0040 // 允许撤销 
FOF_FILESONLY 0x0080 // 使用 *.* 时, 只对文件操作 
FOF_SIMPLEPROGRESS 0x0100 // 简单进度条，意味者不显示文件名。 
FOF_NOCONFIRMMKDIR 0x0200 // 建新目录时不需要用户确定 
FOF_NOERRORUI 0x0400 // 不显示出错用户界面 
FOF_NOCOPYSECURITYATTRIBS 0x0800 // 不复制 NT 文件的安全属性 
FOF_NORECURSION 0x1000 // 不递归目录 
返回值： 
函数成功返回 0 ，失败返回非 0 。 
例子： 
1. 将 C:\Test.txt 拷贝到 D:\ 
SHFILEOPSTRUCT lpsh; 
ZeroMemory(&lpsh,sizeof(lpsh)); 
lpsh.hwnd= HWND_DESKTOP; 
lpsh.fFlags=FOF_NOCONFIRMATION|FOF_SIMPLEPROGRESS ; 
lpsh.wFunc=FO_COPY; // FO_MOVE 则是移动 
lpsh.pFrom= "C:\Test.txt"; 
lpsh.pTo = "D:\"; 
if( 0 != SHFileOperation(&lpsh)) 
{ 
AfxMessageBox("复制文件出错,请检查"); 
return ; 
} 
2. 删除 D:\Test.txt 
SHFILEOPSTRUCT lpsh; 
ZeroMemory(&lpsh,sizeof(lpsh)); 
lpsh.hwnd= HWND_DESKTOP; 
lpsh.fFlags=FOF_NOCONFIRMATION|FOF_SIMPLEPROGRESS ; 
lpsh.wFunc=FO_DELETE; 
lpsh.pFrom= "D:\Test.txt"; 
if( 0 != SHFileOperation(&lpsh)) 
{ 
AfxMessageBox("删除文件出错,请检查"); 
return ; 
} 
3.重命名 
SHFILEOPSTRUCT lpsh; 
ZeroMemory(&lpsh,sizeof(lpsh)); 
lpsh.hwnd= HWND_DESKTOP; 
lpsh.fFlags=FOF_NOCONFIRMATION|FOF_SIMPLEPROGRESS ; 
lpsh.wFunc=FO_RENAME; 
lpsh.pFrom= "D:\Test.txt"; 
lpsh.pTo = "D:\Test2.txt"; 
if( 0 != SHFileOperation(&lpsh)) 
{ 
AfxMessageBox("重命名文件出错!"); 
return ; 
} 
4.VB 
Public Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long 
Public Const FO_COPY = &H2 
Public Const FOF_ALLOWUNDO = &H40 
Public Sub ShellCopyFile(Source As String, Dest As String) 
Dim result As Long 
Dim fileop As SHFILEOPSTRUCT 
With fileop 
.hwnd = 0 
.wFunc = FO_COPY 
.pFrom = Source & vbNullChar & vbNullChar 
.pTo = Dest & vbNullChar & vbNullChar 
.fFlags = FOF_ALLOWUNDO 
End With 
result = SHFileOperation(fileop) 
If result <> 0 Then 
'Msgbox the error that occurred in the API. 
MsgBox Err.LastDllError, vbCritical Or vbOKOnly 
Else 
If fileop.fAnyOperationsAborted <> 0 Then 
MsgBox "Operation Failed", vbCritical Or vbOKOnly 
End If 
End If 
End Sub
总结一下SHFileOperation的用法,希望对大家有用
//删除文件或者文件夹
bool DeleteFile(char * lpszPath)
{
SHFILEOPSTRUCT FileOp={0};
FileOp.fFlags = FOF_ALLOWUNDO |   //允许放回回收站
      FOF_NOCONFIRMATION; //不出现确认对话框
FileOp.pFrom = lpszPath;
FileOp.pTo = NULL;      //一定要是NULL
FileOp.wFunc = FO_DELETE;    //删除操作
return SHFileOperation(&FileOp) == 0;
}
//复制文件或文件夹
bool CopyFile(char *pTo,char *pFrom)
{
SHFILEOPSTRUCT FileOp={0};
FileOp.fFlags = FOF_NOCONFIRMATION|   //不出现确认对话框
      FOF_NOCONFIRMMKDIR ; //需要时直接创建一个文件夹,不需用户确定
FileOp.pFrom = pFrom;
FileOp.pTo = pTo;
FileOp.wFunc = FO_COPY;
return SHFileOperation(&FileOp) == 0;
}
//移动文件或文件夹
bool MoveFile(char *pTo,char *pFrom)
{
SHFILEOPSTRUCT FileOp={0};
FileOp.fFlags = FOF_NOCONFIRMATION|   //不出现确认对话框
      FOF_NOCONFIRMMKDIR ; //需要时直接创建一个文件夹,不需用户确定
FileOp.pFrom = pFrom;
FileOp.pTo = pTo;
FileOp.wFunc = FO_MOVE;
return SHFileOperation(&FileOp) == 0;   
}
//从命名文件或文件夹
bool ReNameFile(char *pTo,char *pFrom)
{
SHFILEOPSTRUCT FileOp={0};
FileOp.fFlags = FOF_NOCONFIRMATION;   //不出现确认对话框
FileOp.pFrom = pFrom;
FileOp.pTo = pTo;
FileOp.wFunc = FO_RENAME;
return SHFileOperation(&FileOp) == 0;   
}
应用举例:
DeleteFile("d:\\PID\0\0");    //删除一个文件夹
DeleteFile("d:\\PID.dsp\0d:\\PID.dsw\0\0"); //删除多个文件
CopyFile("d:\0\0","D:\\MyProjects\\临时程序\0\0");    //把"临时程序"文件夹放到d盘下面
CopyFile("d:\0\0","D:\\MyProjects\\临时程序\\PID.dsp\0D:\\MyProjects\\临时程序\\PID.dsw\0"); //把PID.dsp和PID.dsw俩个文件放到d盘下面
ReNameFile("d:\\NewName","d:\\PID\0\0"); \\把PID文件夹从命名为NewName
注意:,如果你想把"D:\\MyProjects\\临时程序\0\0"的文件夹复制到D盘下,并从命名为NewName,应该这样
CopyFile("d:\\NewName\0\0","D:\\MyProjects\\临时程序\\*.*\0\0"); //把"临时程序"文件夹复制到d盘下并从命名为"NewName"  
下面这个类方便你复制多个文件或文件夹,仅作参考
//连接多个路径的类
class JOINFILEPATH
{
private:
int pos;
char* MultipleFilePath;
public:
JOINFILEPATH()
{
   pos=0;
   MultipleFilePath=new char[MAX_PATH*10];
   memset(MultipleFilePath,0,MAX_PATH*10);
}
~JOINFILEPATH() { delete []MultipleFilePath; }
void join(char *FilePath)
{
   while(*FilePath!='\0')
    MultipleFilePath[pos++]=*FilePath++;
   pos++;
}
char * GetMultipleFilePath() {return MultipleFilePath;}
};
//应用举例:
JOINFILEPATH FilePath;
FilePath.join("D:\\MyProjects\\临时程序\\PID\\PID.dsp");
FilePath.join("D:\\MyProjects\\临时程序\\PID\\PID.dsw");
CopyFile("d:\0\0",FilePath.GetMultipleFilePath());
1 pFrom和pTo最好以\0\0结尾(把存放路径的字符串初始化为0),不然有可能会出错,中间的每一个路径用\0隔开
2 pFrom所指向的文件或文件夹(可以多个)会被复制或移动到pTo所指向的文件夹下面(假如文件夹不存在会询问是否创建,当然你也可以选择直接创建)
参数详解:
Typedef struct _ShFILEOPSTRUCT
{
HWND hWnd; //消息发送的窗口句柄；
UINT wFunc; //操作类型
LPCSTR pFrom; //源文件及路径
LPCSTR pTo; //目标文件及路径
FILEOP_FLAGS fFlags; //操作与确认标志
BOOL fAnyOperationsAborted; //操作选择位
LPVOID hNameMappings; //文件映射
LPCSTR lpszProgressTitle; //文件操作进度窗口标题
}SHFILEOPSTRUCT, FAR * LPSHFILEOPSTRUCT;
　　在这个结构中，hWnd是指向发送消息的窗口句柄，pFrom与pTo是进行文件操作的源文件名和目标文件名，它包含文件的路径，对应单个文件的路径字符串，或对于多个文件，必须以NULL作为字符串的结尾或文件路径名之间的间隔，否则在程序运行的时候会发生错误。另外，pFrom和pTo都支持通配符*和？，这大大方便了开发人员的使用。例如，源文件或目录有两个，则应是：char pFrom[]="d:\\Test1\0d:\\Text.txt\0"，它表示对要D：盘Test目录下的所有文件和D：盘上的Text.txt文件进行操作。字符串中的"\"是C语言中的'\'的转义符，'\0'则是NULL。wFunc 是结构中的一个非常重要的成员，它代表着函数将要进行的操作类型,它的取值为如下：
　　FO_COPY: 拷贝文件pFrom到pTo 的指定位置。
　　FO_RENAME: 将pFrom的文件名更名为pTo的文件名。
　　FO_MOVE: 将pFrom的文件移动到pTo的地方。
　　FO_DELETE： 删除pFrom指定的文件。
　　使用该函数进行文件拷贝、移动或删除时，如果需要的时间很长，则程序会自动在进行的过程中出现一个无模式的对话框（Windows操作系统提供的文件操作对话框），用来显示执行的进度和执行的时间，以及正在拷贝、移动或删除的文件名，此时结构中的成员lpszProgressTitle显示此对话框的标题。fFlags是在进行文件操作时的过程和状态控制标识。它主要有如下一些标识，也可以是其组合：
　　FOF_FILESONLY：执行通配符，只执行文件；
　　FOF_ALLOWUNDO：保存UNDO信息，以便在回收站中恢复文件；
　　FOF_NOCONFIRMATION：在出现目标文件已存在的时候，如果不设置此项，则它会出现确认是否覆盖的对话框，设置此项则自动确认，进行覆盖，不出现对话框。
　　FOF_NOERRORUI：设置此项后，当文件处理过程中出现错误时，不出现错误提示，否则会进行错误提示。
　　FOF_RENAMEONCOLLISION：当已存在文件名时，对其进行更换文提示。
　　FOF_SILENT：不显示进度对话框。
　　FOF_WANTMAPPINGHANDLE：要求SHFileOperation()函数返回正处于操作状态的实际文件列表，文件列表名柄保存在hNameMappings成员中。
　　SHFILEOPSTRUCT结构还包含一个SHNAMEMAPPING结构的数组，此数组保存由SHELL计算的每个处于操作状态的文件的新旧路径。
　　在使用该函数删除文件时必须设置SHFILEOPSTRUCT结构中的神秘FOF_ALLOWUNDO标志，这样才能将待删除的文件拷到Recycle Bin，从而使用户可以撤销删除操作。需要注意的是，如果pFrom设置为某个文件名，用FO_DELETE标志删除这个文件并不会将它移到Recycle Bin，甚至设置FOF_ALLOWUNDO标志也不行，在这里你必须使用全路径名，这样SHFileOperation才会将删除的文件移到Recycle Bin。
转自http://blog.csdn.net/jhb92/archive/2007/04/13/1563452.aspx
Shell的文件操作函数
SHFileOperation
功能:
1.复制一个或多个文件
2.删除一个或多个
3.重命名文件
4.移动一个或多个文件
有一样的Win32API功能函数是:
CopyFile(),DeleteFile(),MoveFile()
MoveFile可以对文件重命名!
Win32 API 的层次比SHFileOperation低
SHFileOperation
的重要参数
1.wFunc //对pFrom pTo要执行的操作
2.fFlags //影响对wFunx的操作
3.hNameMappings   //有系统填充,和你也可以填充
4.lpszProgressTitle
pFrom pTo 在结尾是两个'\0\0'
通常用一个'\0'，这样会失败的!!
当FOF_MULTIDESTFILES
szPFrom[lstrlen(szPFrom)+1]=0
szPFrom:必须先确定他所指定的文件存在!
可以是单个文件名,*.*,或包含统配符的文件名
注意必须是文件名,不是文件所在的文件夹名
szSource:可以是一个目录，如果不是目录，但又有
多个文件，那么必须和szPFrom的每一个文件对应,还要指定
FOF_MULTIDETFILES标志
Source and Target
多个文件---> 一个文件夹
许多单独的文件---->一个文件夹
单独文件--->单独文件
许多单独的文件---->许多单独的文件
单独文件：知道名字的文件
多个文件:带有统配符的文件
注意到source中没有对文件夹的操作!!
!!!!
SHFileOperation能操作网络上的文件
如果你想将本地文件复制到192.168.1.99
那么只要在192.168.1.99上共享123目录
然后将pTo设置为\\192.168.1.99\123
就可以了
但不要设置为\\192.168.1.99
对hNameMappings操作是Undocumented!!
如果没有指定hNameMappings
那么hNameMappings一直是NULL
只有当某种操作(copy,move,rename)引起了文件名冲突了，hNameMappings才不是NULL!!!
当第一次copy某些文件到空目录中时hNameMappings一定是NULL
所以hNameMappings只是内存中的一块地区用来让Explorer.exe保存被重命名的文件,以避免文件名冲突!
上面知道了如何才能使hNameMappings有效
现在如何使用hNameMappings,及其所指的结构大小?并取得这个内存块的内容呢?
hNameMappings 是简单LPVOID无法使用loop
要使用hNameMappings，必须定义一个结构体
struct HANDLETOMAPPINGS {
    UINT              uNumberOfMappings; // number of mappings in array
    LPSHNAMEMAPPING   lpSHNameMapping;    // pointer to array of mappings
};
但是可以写一个Enumerate function to enumerate lpSHNameMapping指向的内存块,并且是让Window自己调用我的,不是我主动调用,象Loop
相关联接:
Q154123:File Name Mapping with Windows NT 4.0 Shell
Q133326:SHFILEOPSTRUCT pFrom and pTo Fields Incorrect
Q142066:PRB: SHGetNameMappingPtr() and SHGetNameMappingCount()
Manipulating Files with the SHFileOperation Function in Visual Basic 4.0
FOF_SILENT //不产生正在复制的对话框
FOF_NOCONFIRMMKDIR//如果目的目录不存在，就默认创建
FOF_NOCONFIRMATION //不出现确认文件替换对话框(Confirmation Dialog)(默认替换原来的文i件)
FOF_NOERRORUI//不出现错误对话框
最好不要同时使用FOF_NOERRORUI,FOF_NOCONFIRMMKDIR
因为FOF_NOCONFIRMMKDIR屏蔽了missing directory Error
但FOF_NOERROR又屏蔽了missing directory Error,那么在同时使用FOF_NOERRORUI,FOF_NOCONFIRMMKDIR
时也阻止了新目录安静(没有用户确认要产生新目录的对话框)的产生!!
那么如何同时使用FOF_NOERRORUI,FOF_NOCONFIRMMKDIR?
就必须先确认pTo所指定的目录存在即可
BOOL MakeSureDiretoryPathExists(LPCSTR DirPath);
使用它要包含imagehlp.h和imagehlp.lib
如何判断同时存在FOF_NOERRORUI,FOF_NOCONFIRMMKDIR
=====
因此本人注释:如果出现弹出"无法删除 文件无法:读取源文件或磁盘文.",
可:FOF_NOCONFIRMATION|FOF_NOCONFIRMMKDIR|FOF_NOERRORUI
=====
FOF_RENAMEONCOLLISION//有重复文件时自动重命名
能产生对话框的标志:
FOF_SILENT             //progress dialog           
FOF_RENAMEONCOLLISION //replace dialog
FOF_NOCONFIRMATION     //confirmation dialog
FOF_NOCONFIRMMKDIR     //asks for your permission to create a new folder
FOF_NOERRORUI          //error message
FOF_ALLOWUNDO     //将文件放入回收站,否则直接删除，一般这个最好做默认
 ///////////////////////////////////////////////////
百度收索到：
1 SHFILEOPSTRUCT详解 
   与所有仅使用数据结构作为输入参数的函数一样，SHFileOperation()函数是一个相当灵活的例程。通过以适当的方式组合各种标志，和使用(或不使用)各个SHFILEOPSTRUCT结构的成员，它可以执行许多操作。下面就让我们来看一看这个结构中每一个成员所起的的作用：
Hwnd
 由这个函数生成的所有对话框的父窗口Handle。
wFunc
 表示要执行的操作
pFrom
 含有源文件名的缓冲
pTo
 含有目标文件名的缓冲(不考虑删除的情况)
fFlags
 能够影响操作的标志
fAnyOperationsAborted
 包含TRUE或FALSE的返回值。它依赖于是否在操作完成之前用户取消了操作。通过检测这个成员，你就可以确定操作是正常完成了还是被手动中断了。
hNameMappings
 资料描述它为包含SHNAMEMAPPING结构数组的文件名映射对象的Handle。
lpszProgressTitle
 一个在一定情况下用于显示对话框标题的字符串。
 抑制的对话框
 相关性与优先级
FOF_MULTIDESTFILES
 None
 None
FOF_FILESONLY
 None
 None
FOF_SILENT 
 如果设置，进度对话框不显示。
 优先于FOF_SIMPLEPROGRESS标志。
FOF_SIMPLEPROGRESS
 None
 为FOF_SILENT标志所抑制。
FOF_RENAMEONCOLLISION
 如果设置了这个标志，当被移动或拷贝的文件与已存在文件同名时置换对话框不会出现。
 名字冲突时，如果FOF_NOCONFIRMATION标志设置，则操作继续。
如果二者都设置了，则它优先于FOF_NOCONFIRMATION。即，文件以给定的新名字复制，而不是覆盖。
FOF_NOCONFIRMATION
 如果设置，确认对话框在任何情况下都不出现。
 名字冲突时，引起文件覆盖，除非设置了FOF_RENAMEONCOLLISION标志。
FOF_NOCONFIRMMKDIR 
 抑制请求建立新文件夹的对话框
 缺省目录作为严重错误产生一个错误消息框。
建立目录的确认对话框作为错误消息框是否显示依赖于FOF_NOERRORUI的设置。
FOF_NOERRORUI 
 抑制所有错误消息框。
 优先于前一个标志。如果设置，则，缺省目录引起不被处理的异常，并且返回错误码。
FOF_SILENT
0x0004
 这个操作不回馈给用户，就是说，不显示进度对话框。相关的消息框仍然显示。
FOF_NOCONFIRMATION
0x0010
 这个标志使函数对任何遇到的消息框都自动回答Yes。
FOF_ALLOWUNDO
 0x0040
 如果设置，这个标志强迫函数移动被删除的文件到‘回收站’中。否则，文件将被物理地从磁盘上删除。
FOF_FILESONLY
0x0080
 设置这个标志导致函数仅仅删除文件，跳过目录项。它仅仅应用于指定通配符的情况。
FOF_SIMPLEPROGRESS
0x0100
 这导致简化用户界面。使之只有动画而不报告被删除的文件名。代之的是显示lpszProgressTitle成员中指定的文字。
FOF_NOERRORUI
0x0400 
 如果设置了这个标志，任何发生的错误都不能使消息框显示，而是程序中返回错误码。 
FOF_SIMPLEPROGRESS或FOF_SILENT隐藏正在被删除的文件名，
通过设置 FOF_FILESONLY，仅仅删除文件。
//
FOF_ALLOWUNDO，它允许程序员决定文件是否一次就全部删除，或存储到‘回收站
’中等候可能的恢复。如果FOF_ALLOWUNDO被设置，文件则被移动到回收站，并且
这个操作可以被Undo(尽管可以手动Undo)。
 

