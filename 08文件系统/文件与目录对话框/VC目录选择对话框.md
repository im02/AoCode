打开目录选择对话框需要使用API函数SHBrowseForFolder来实现。
 
函数原型为:
LPITEMIDLIST SHBrowseForFolder(LPBROWSEINFO lpbi);
其中LPBROWSEINFO为BROWSEINFO结构的指针。
BROWSEINFO结构如下:
typedef struct _browseinfo
{
    HWND hwndOwner;
    LPCITEMIDLIST pidlRoot;
    LPSTR pszDisplayName;
    LPCSTR lpszTitle;
    UINT ulFlags;
    BFFCALLBACK lpfn;
    LPARAM lParam;
    int iImage;
} BROWSEINFO;
 
使用例子如下：
void CTestDirDlg::OnButton1()
{
 BROWSEINFO bi;
 char path[MAX_PATH];  bi.hwndOwner = NULL;
 bi.pidlRoot = NULL;
 bi.pszDisplayName = Buffer; //此参数如为NULL则不能显示对话框
 bi.lpszTitle = "选择一个目录";
 bi.ulFlags = BIF_RETURNONLYFSDIRS;
 bi.lpfn = NULL;
 bi.iImage = 0;   //初始化入口参数bi结束
 LPITEMIDLIST pIDList = SHBrowseForFolder(&bi);//调用显示选择对话框
 if(pIDList)
  {
    SHGetPathFromIDList(pIDList, path);
    //取得文件夹路径到path里
    m_strPathName = path;//将路径保存在一个CString对象里，m_strPathName为编辑框绑定的一个值类
                         //型变量
   }
   UpdateData(FALSE);    //将选择的文件夹路径显示在编辑框中
}