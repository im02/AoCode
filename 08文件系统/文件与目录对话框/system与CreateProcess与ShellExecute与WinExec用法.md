system使用的批处理,有DOS黑框,功能不强

CreateProcess以前讲了很多了,可以打开文件夹中指定的文件

WinExec打开文件目录失败.其它没试过

ShellExecute第1次用,除了可以打开指定文件外,还能打开指定的目录.

下面是摘抄的网上的ShellExecute用法:

ShellExecute的用法 http://www.xici.net/main.asp?url=/u364344/d6799224.htm 
02-08-28 15:44   发表于：《C++Builder论坛》　分类：未分类

Q: 如何打开一个应用程序？ 
ShellExecute(this->m_hWnd,"open","calc.exe","","", SW_SHOW );
或
ShellExecute(this->m_hWnd,"open","notepad.exe",
　　"c:\\MyLog.log","",SW_SHOW );
As you can see, I haven't passed the full path of the programs.
Q: 如何打开一个同系统程序相关连的文档？
ShellExecute(this->m_hWnd,"open",　　"c:\\abc.txt","","",SW_SHOW );
Q: 如何打开一个网页？
ShellExecute(this->m_hWnd,"open",　　"http://www.google.com","","", SW_SHOW );
Q: 如何激活相关程序，发送EMAIL？
ShellExecute(this->m_hWnd,"open",
　　"mailto:nishinapp@yahoo.com","","", SW_SHOW );
Q: 如何用系统打印机打印文档？
ShellExecute(this->m_hWnd,"print",
　　"c:\\abc.txt","","", SW_HIDE);
Q: 如何用系统查找功能来查找指定文件？
ShellExecute(m_hWnd,"find","d:\\nish",
　　NULL,NULL,SW_SHOW);
Q: 如何启动一个程序，直到它运行结束？
SHELLEXECUTEINFO ShExecInfo = {0};
ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
ShExecInfo.hwnd = NULL;
ShExecInfo.lpVerb = NULL;
ShExecInfo.lpFile = "c:\\MyProgram.exe";　　　　　　
ShExecInfo.lpParameters = "";　　
ShExecInfo.lpDirectory = NULL;
ShExecInfo.nShow = SW_SHOW;
ShExecInfo.hInstApp = NULL;　　　
ShellExecuteEx(&ShExecInfo);
WaitForSingleObject(ShExecInfo.hProcess,INFINITE);
或：
PROCESS_INFORMATION ProcessInfo;
STARTUPINFO StartupInfo; //This is an [in] parameter
ZeroMemory(&StartupInfo, sizeof(StartupInfo));
StartupInfo.cb = sizeof StartupInfo ; //Only compulsory field
if(CreateProcess("c:\\winnt\\notepad.exe", NULL,
　　NULL,NULL,FALSE,0,NULL,
　 NULL,&StartupInfo,&ProcessInfo))
{
　 WaitForSingleObject(ProcessInfo.hProcess,INFINITE);
　　CloseHandle(ProcessInfo.hThread);
　　CloseHandle(ProcessInfo.hProcess);
} 
else
{
　　MessageBox("The process could not be started...");
}
Q: 如何显示文件或文件夹的属性？
SHELLEXECUTEINFO ShExecInfo ={0};
ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
ShExecInfo.fMask = SEE_MASK_INVOKEIDLIST ;
ShExecInfo.hwnd = NULL;
ShExecInfo.lpVerb = "properties";
ShExecInfo.lpFile = "c:\\"; //can be a file as well
ShExecInfo.lpParameters = "";
ShExecInfo.lpDirectory = NULL;
ShExecInfo.nShow = SW_SHOW;
ShExecInfo.hInstApp = NULL;
ShellExecuteEx(&ShExecInfo);
