VC++文件对话框、目录对话框的实现，CFileDialog SHGetPathFromIDList


文件对话框 CFileDialog：
void CDirXXXDlg::OnXXX() 
{
// TODO: Add your control notification handler code here
char szFilter[]=_T("Text Files(*.txt)|*.txt|C Source Files(*.c)|*.c|C++ Source Files(*.cpp)|*.cpp|All Files (*.*)|*.*||");
char szFullPath[MAX_PATH];//完整路径
char szFileName[MAX_PATH];//文件名
char szExtName[MAX_PATH];//扩展名
char szTileName[MAX_PATH];//文件名（不包含扩展名）
char szFileDir[MAX_PATH];//文件所在目录（最后包含反斜杠）
CFileDialog Filedlg(TRUE,NULL,NULL,NULL,szFilter);
if(Filedlg.DoModal()==IDCANCEL)
return;
    strcpy(szFullPath,Filedlg.GetPathName());
strcpy(szFileName,Filedlg.GetFileName());
strcpy(szExtName,Filedlg.GetFileExt());
strcpy(szTileName,Filedlg.GetFileTitle());
//根据szFullPath计算文件目录
int nLen=strlen(szFullPath);
char* pPath=&szFullPath[nLen];
//倒数查找最后一个"\"
while (pPath!=NULL && *pPath!='\\')
{
pPath--;
}
//得到最后一个反斜扛之后再往后加1,并把它设置为0,这样后面的数据对于字符数组就会忽略
pPath++;
*pPath=0;
strcpy(szFileDir,szFullPath);
}


目录浏览对话框：
void CDirxxxDlg::Onxxx() 
{
// TODO: Add your control notification handler code here
char szDir[MAX_PATH];
char szDirPath[MAX_PATH];//保存实际浏览目录路径包含"\"
BROWSEINFO bi;
ITEMIDLIST *pidl;
bi.hwndOwner = this->m_hWnd;
bi.pidlRoot = NULL;
bi.pszDisplayName = szDir;
bi.lpszTitle = "请选择目录";
bi.ulFlags = BIF_STATUSTEXT | BIF_RETURNONLYFSDIRS;
bi.lpfn = NULL;
bi.lParam = 0;
bi.iImage = 0;
pidl = SHBrowseForFolder(&bi);
if(pidl == NULL)  return;
if(!SHGetPathFromIDList(pidl, szDir))
return;
else
strcpy(szDirPath,szDir);

int nLen =strlen(szDirPath);
char* p = &szDirPath[nLen-1];
if (*p=='\\')//根目录的情况
return;
else
strcat(szDirPath,"\\");
}
