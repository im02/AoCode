准备工作：用windows自带的记事本工具，创建一个文件，里面写几句话。然后使用另存为按钮，分别另存为为：ANSI、UTF-8、Unicode、Unicode Big endian四种格式。

主程序如下：

创建一个基于对话框的MFC程序，在对话框中拖拽一个按钮，双击后添加如下代码到这个按钮响应的函数中：

void CFileCodeDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	//打开一个文件
	CFileDialog dlg(TRUE,"Txt",NULL,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"(*.txt)|*.txt||");
	if (dlg.DoModal()==IDOK)
	{
		CString strFilePath = dlg.GetPathName();
		CFile file;
		file.Open(strFilePath,CFile::modeRead);
		if (!file)
		{
			AfxMessageBox("呵呵哒!");
		}
		//读取文件头
		unsigned char str[1024];
		unsigned short head;
		size_t len = sizeof(head);
		file.Read(&head,sizeof(head));
		switch(head)
		{
		case 0xbbef:
			AfxMessageBox("UTF-8");
			break;
		case 0xfffe:
			AfxMessageBox("unicode Big Endian格式");
			break;
		case 0xfeff:
			AfxMessageBox("unicode格式");
			break;		
		case 0xd2ce:
			AfxMessageBox("ANSI 格式");
			break;
		default:
			AfxMessageBox("无法判断格式");
			break;
		}
		file.Close();
	}
	
}


