有时候想将一个VC工程转换为UNICODE版本，却不知怎样转换，下面提供一个转换的方法：
        1.为了成功编译支持 UNICODE 的 MFC 程序，必须使用 MFC 的 UNICODE 版本库。该库在定制安装Visual C++ 时是个可选安装项。按以下步骤选择：选中Microsoft Visual C++6.0-->Change Option...-->VC++ MFC and Template LIbraries-->Change Option...-->MS Foundation Class Libraries-->Change Option...-->选中Static Libraries for Unicode和Share Libraries for Unicode.
       2. 启动VC开发环境，打开要转换的工程，在project->settings->C/C++的属性页中的Preprocessor中，删除_MBCS写入UNICODE,_UNICODE。
       3.在link属性页中Category中选择output，在Entry-Pointsymbol中添加wWinMainCRTStartup。
　　设置完成，重新编译工程即可。
