
### 源码名称：html自动提交表单、自动跳转标签 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
html自动提交表单、自动跳转标签
```
#### 下载：无
#### 使用方法：


``` html
自动提交form表单
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body οnlοad="document.forms[0].submit();">
<form  id=form1 name=form1 action="http://baidu.com" method=post>
<input type=hidden id=siteCode name=siteCode value='ss'>
</body>
</html>
自动跳转标签

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<script language="javascript" type="text/javascript">
			 location.href = "https://www.baidu.com/";
		</script>
	</head>
</html>


```

