
### 源码名称：php获取URL各部分参数
#### 修改时间：20180212
#### 语言：php
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
php获取URL各部分参数
```
#### 下载：无
#### 使用方法：


``` php
URL处理几个关键的函数parse_url、parse_str与http_build_query

parse_url()

该函数可以解析 URL，返回其组成部分。它的用法如下：

array parse_url(string $url)

此函数返回一个关联数组，包含现有 URL 的各种组成部分。如果缺少了其中的某一个，则不会为这个组成部分创建数组项。组成部分为：

scheme - 如 http 
host - 如 localhost
port - 如 80
user 
pass 
path - 如 /parse_str.php
query - 在问号 ? 之后  如 id=1&category=php&title=php-install
fragment - 在散列符号 # 之后 
此函数并不意味着给定的 URL 是合法的，它只是将上方列表中的各部分分开。parse_url() 可接受不完整的 URL，并尽量将其解析正确。此函数对相对路径的 URL 不起作用。


<?php
    $url = "http://52php.cnblogs.com/welcome/";
    $parts = parse_url($url);
     
    print_r($parts);
?>
程序运行结果如下：


Array
(
    [scheme] => http
    [host] => 52php.cnblogs.com
    [path] => /welcome/
)

<?php
    $url = 'http://username:password@hostname/path?arg=value#anchor';
    print_r(parse_url($url));
    echo '<br />';
    echo parse_url($url, PHP_URL_PATH);
?>
程序输出：


Array
(
    [scheme] => http
    [host] => hostname
    [user] => username
    [pass] => password
    [path] => /path
    [query] => arg=value
    [fragment] => anchor
)
可以看到，可以很容易分解出一个URL的各个部，那如果要拿指定的部分出来的话也很容易，如：


echo parse_url($url, PHP_URL_PATH);
就是在第二个参数中，设定如下的参数：PHP_URL_SCHEME, PHP_URL_HOST, PHP_URL_PORT, PHP_URL_USER, PHP_URL_PASS, PHP_URL_PATH, PHP_URL_QUERY or PHP_URL_FRAGMENT。

 

 

parse_str()

parse_str用来解析URL中的查询字符串，即可以通过$_SERVER['QUERY_STRING']取得的字符串值，假如我们请求的URL是 http://localhost/parse_str.php?id=1&category=php&title=php-install，那么$_SERVER['QUERY_STRING']返回的值为id=1&category=php&title=php-install，而这种形式的字符串恰巧可以使用parse_str解析成关联数组的形式。

用法如下：

void parse_str(string $str [, array &$arr ])

该函数接收两个参数，$str为需要解析的字符串，而可选参数$arr为解析之后生成的数组值所存放的变量名，如果忽略可选参数，那么可以直接调用类似$id、$category、$title的变量。下面的脚本模拟了GET请求。


<?php
<a href="http://localhost/parse_str.php?id=1&category=php&title=php-install">Click Here</a>
$query_str = $_SERVER['QUERY_STRING'];
parse_str($query_str); /* 这种方式可以直接使用变量$id, $category, $title */
parse_str($query_str, $query_arr);
?>
<pre><?php print_r($query_arr); ?></pre>
<p><?php echo $id; ?></p>
<p><?php echo $category; ?></p>
<p><?php echo $title; ?></p>
?>
 
/* 运行结果 */
Array
(
    [id] => 1
    [category] => php
    [title] => php-install
)
1
php
php-install
 
```
