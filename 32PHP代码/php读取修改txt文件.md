
### 源码名称：php读取修改txt文件 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
php读取修改txt文件
```
#### 下载：无
#### 使用方法：



//txt文件中只有一行数据 读

![img](https://images.cnblogs.com/OutliningIndicators/ContractedBlock.gif) View Code

```php
$fp = fopen("password.txt", "r");
if($fp)
{
     $pwd = fgets($fp);
}
else
{
     echo "打开文件失败";
}
fclose($fp);
```



//txt文件中只有一行数据 写

![img](https://images.cnblogs.com/OutliningIndicators/ContractedBlock.gif) View Code

```php
$fp = fopen("password.txt", "w");//文件被清空后再写入
if($fp)
{
     $flag=fwrite($fp,$password);
     if(!$flag)
     {
          echo "写入文件失败<br>";
          break;
     }
}
fclose($fp);
```



//txt文件中有多行数据  读 以数组的形式

![img](https://images.cnblogs.com/OutliningIndicators/ContractedBlock.gif) View Code

```php
$path="wifi_customer_settings.txt";
$body = file_get_contents($path);
if( file_exists( $path ) )
{
    $body = file_get_contents($path);//转为数组
//echo "<script language=javascript>alert('文件存在');</script>";
}
else
{
   // echo "<script language=javascript>alert('文件不存在 $path');</script>";
}
$cbody = file($path);
//print_r($cbody);
$wifissid=$cbody[0];
$wifipwd=$cbody[1];
$wifissid=str_replace('wifiSSID=','' ,$wifissid);//文本替换 将$wifissid值的wifiSSID替换为' '
$wifipwd=str_replace('wifiPWD=','' ,$wifipwd);
```

//txt文件中有多行数据 写  以数组的形式

![img](https://images.cnblogs.com/OutliningIndicators/ContractedBlock.gif) View Code

```php
$path="wifi_customer_settings.txt";
$wifissid2=$_POST['wifissid'];
$wifipwd2=$_POST['wifipwd'];
$arr=array("wifiSSID=".$wifissid2,"\n","wifiPWD=".$wifipwd2);
$fp=fopen($path, 'w');
fputs($fp,$arr[0]); 
fputs($fp,$arr[1]);
fputs($fp,$arr[2]);
fclose($fp);
```

