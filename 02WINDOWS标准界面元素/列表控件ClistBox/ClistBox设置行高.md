
### 源码名称：ClistBox设置行高
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
ClistBox设置行高
```
#### 下载：无
#### 使用方法：


``` c++
CImageList imgList; //为ClistCtrl设置一个图像列表，以设置行高
imgList.Create(IDB_BITMAP4, 16, 1, RGB(255, 255, 255)); // IDB_BITMAP4 是 16*16的 所以行高16像素

CListCtrl &listCtrl=(CListCtrl&)GetListCtrl();
listCtrl.SetImageList(&imgList,LVSIL_SMALL);

目前我只用过这个办法，可能还有别的办法，估计比较麻烦

//自己方法总结

在 oninitial：

 m_cListResult.SetItemHeight(0,30);

HBRUSH CATE1368ManuFocusDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
 HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
 
 // TODO: Change any attributes of the DC here
 if((pWnd->GetDlgCtrlID()==IDC_LIST_RESULT))
 {   
  //pDC->SetTextColor(RGB(255,0,0));   
  CFont   font;   
  LOGFONT   lf;   
  memset(&lf,   0,   sizeof(LOGFONT));   //   清零   
  lf.lfHeight = 30;  //   设置字体高度   
  //lf.lfWidth = 60;
  //lf.lfWeight = 60;
  strcpy(lf.lfFaceName,   "Arial");                 //   设置字形   
  font.CreateFontIndirect(&lf);     //   生成字体   
  pDC->SelectObject(&font);   
 }

 
 // TODO: Return a different brush if the default is not desired
 return hbr;
}
```
