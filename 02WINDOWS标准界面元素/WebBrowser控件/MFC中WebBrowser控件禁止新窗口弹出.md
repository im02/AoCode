
### 源码名称：MFC中WebBrowser控件禁止新窗口弹出 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
MFC中WebBrowser控件禁止新窗口弹出
```
#### 下载：无
#### 使用方法：


``` c++
我在XP SP3上测试，WebBrowser的OnNewWindow3事件中处理是无效的，不知道人家说XP SP2有效是怎么回事。

那种创建一个隐藏窗口出来的办法，我个人反正是不愿意去用，总之感觉不太好，而且据说这是目前唯一的解决办法。

 

后来实在没办法了，只能往土办法去想了，我们知道IE中有状态栏这个东西，当鼠标指向某个链接时，状态栏的文字就变成链接地址了。

经过测试之后，确实可以用WM_GETTEXT来获取到链接，这就解决了OnNewWindow2中获取不到新窗口的链接的问题了。

 

但是IE可以有状态栏，WebBrowser中我找了一下，没有发现能够显示状态栏的操作，在spy++中查看，也不像IE那样有状态栏的窗口。所以，我又仔细研究了一下WebBrowser关于状态栏的事件。后来发现StatusTextChange中其实就可以直接获取到状态栏的文字了。

 

于是，写代码的事情就很简单了，而且是完美的解决问题，不管是js的   window.open    还是一个简单的新窗口链接。

void CMWebDlg::OnNewWindow2Explorer1(LPDISPATCH FAR* ppDisp, BOOL FAR* Cancel)
{
 VARIANT vars;
 m_Web.Navigate(strURL,NULL,NULL,&vars,&vars);
 *Cancel =TRUE;
}

void CMWebDlg::OnStatusTextChangeExplorer1(LPCTSTR Text) 
{
 strURL=Text;
}

在类中或者CPP中  CString strURL; 一下即可。
```
