
### 源码名称：MFC树形控件CTreeCtrl常用方法 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
MFC树形控件CTreeCtrl常用方法
 1. 新建树形控件
 2. 插入节点
 3. 获取选中节点名称
 4. 根据名称查找节点
 5. 解决初始化SetCheck无效问题



```
#### 下载：无
#### 使用方法：


``` c++
 1. 新建树形控件
     BOOL Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID );
    返回值：如果初始化成功则返回非零值；否则返回0
    参数说明
    dwStyle：指定tree view控件的风格。可以对这个控件使用tree view控件风格的任意组合。
    rect：   指定tree view控件的尺寸和位置。此参数可以是一个CRect对象或一个RECT结构。
    pParentWnd：指定tree view控件的父窗口，通常是一个CDialog。它不能是NULL。
    nID：指定tree view控件的ID。
   风格说明：
      TVS_HASLINES tree view控件的子项与它们的父项之间用线连接。
      TVS_LINESATROOT tree view控件用线连接子项和根项。
      TVS_HASBUTTONS tree view在每一个父项的左边添加一个按钮。
      TVS_EDITLABELS tree view控件允许用户编辑tree view项的标签。
      TVS_SHOWSELALWAYS 当tree view失去焦点时，使被选择的项仍然保持被选择
      TVS_DISABLEDRAGDROP 该tree view控件被禁止发送TVN_BEGINDRAG通知消息。
      TVS_NOTOOLTIPS tree view控件使用工具提示。
      TVS_SINGLEEXPAND 当使用这个风格时，改变在tree view中的选择将导致正被选择的项展开，而没有被选择的项收缩。如果用鼠标单击被选择的项，并且该项是关闭的，则该项就会展开。如果该被选择的项被单击时是打开的，则它就会收缩。

 2. 插入节点
     HTREEITEM hRoot;     // 树的根节点的句柄   
    HTREEITEM hCataItem; // 可表示任一分类节点的句柄
    hRoot = m_Tree.InsertItem(_T("Root"),0,0);
    for(int i=0; i < 5; i++)
    {
         CString str;
         str.Format(_T("Item%d"),i+1);
         hCataItem = m_Tree.InsertItem(str, 1, 1, hRoot, TVI_LAST);         
    }   

 3. 获取选中节点名称
 void GetCheckNode( HTREEITEM hRoot )
{
    // 如果不是叶子节点
    if(TRUE == m_Tree.ItemHasChildren(hRoot))
    {
        if(TRUE == m_Tree.GetCheck(hRoot))
        {
            // 插入所有页节点
            InsertAllLeaves(hRoot);
        }
        else
        {
            // 查询所有节点，递归
            HTREEITEM hChild = m_Tree.GetChildItem(hRoot);
            while(NULL != hChild)
            {
                GetCheckNode(hChild);
                hChild = m_Tree.GetNextSiblingItem(hChild);
            }
        }
    }
    else // 是叶子节点
    {
        // 被选中
        if(TRUE == m_tTree.GetCheck(hRoot))
        {
            CString well = m_Tree.GetItemText(hRoot);
        }
    }
}


void InsertAllLeaves( HTREEITEM hRoot )
{
    HTREEITEM hChild = m_Tree.GetChildItem(hRoot);
    while(NULL != hChild)
    {
        if(TRUE == m_Tree.ItemHasChildren(hChild))
        {
            InsertAllLeaves(hChild);
        }
        else
        {
            CString well = m_Tree.GetItemText(hChild);
        }
        hChild = m_Tree.GetNextSiblingItem(hChild);
    }
}

 4. 根据名称查找节点
 HTREEITEM  finditem(HTREEITEM  item, CString strtext)   
{  
    HTREEITEM  hfind;  
    //空树，直接返回NULL
    if(item ==  NULL)  
        return  NULL;  
    //遍历查找
    while(item!=NULL)  
    {  
        //当前节点即所需查找节点
        if(m_Tree.GetItemText(item) == strtext)  
            return   item;  
        //查找当前节点的子节点
        if(m_Tree.ItemHasChildren(item))  
        {  
            item   =   m_Tree.GetChildItem(item); 
            //递归调用查找子节点下节点
            hfind   =   finditem(item,strtext); 
            if(hfind)  
            {  
                return  hfind;  
            }  
            else   //子节点中未发现所需节点，继续查找兄弟节点
            {
                item = m_Tree.GetNextSiblingItem(m_ShowObjectTree.GetParentItem(item));  
            }
        }  
        else{   //若无子节点，继续查找兄弟节点
            item = m_Tree.GetNextSiblingItem(item);  
        }  
    }  
    return item;  
}

 5. 解决初始化SetCheck无效问题
 （1）问题：CTreeCtrl通过两个函数SetCheck / GetCheck来设置和获取指定节点的选择状态。但是奇怪的是，在对话框OnInitDialog中按照常规的方法使用了SetCheck， 
最后CTreeCtrl并没有显示节点被选中。 
（2）问题原因：是树控件在创建自己的时候，并没有加载所需的图标列表，而是在显示的时候，发现需要后，它才加载，并且重新复位了每个节点的选择状态。它之所以这 
么做，我想可能是出于效率方面的考虑。也就是说，如果用户没有添加TVS_HASBUTTONS，那么它就不需要这个图标列表了。 
（3）所以知道了问题的所在，那么鉴于在对话框初始化的时候，设置树控件的数据和选择状态是一个常规的做法，我就不打算修改这些代码的位置，而采用提前给它加载图 
标列表的方法。方法如下：

    HICON icon[4];
      icon[0]=AfxGetApp()->LoadIcon(IDI_ICON2);
      icon[1]=AfxGetApp()->LoadIcon(IDI_ICON3);

      CImageList *ImageList4Tree = new CImageList;

      ImageList4Tree->Create(16,16,0,4,4); //16,16为图标分辩率，4,4为该list最多能容纳的图标数
      for(int i=0;i<2;i++)
      {
        ImageList4Tree->Add(icon[i]); //读入图标
      }

      m_Tree.SetImageList(ImageList4Tree,TVSIL_NORMAL);
      m_Tree.SetCheck(hCataItem);

```
