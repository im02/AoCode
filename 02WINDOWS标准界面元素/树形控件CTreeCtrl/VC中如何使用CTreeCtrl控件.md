VC中树形控件（CTreeCtrl）的使用默认分类 2010-11-11 08:58:57 阅读121 评论0   字号：大中小 订阅 .






树形控件可以用于树形的结构，其中有一个根接点(Root)然后下面有许多子结点，而每个子结点上有允许有一个或多个或没有子结点。MFC中使用CTreeCtrl类来封装树形控件的各种操作。通过调用
BOOL Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID );
创建一个窗口，dwStyle中可以使用以下一些树形控件的专用风格：

TVS_HASLINES 在父/子结点之间绘制连线 
TVS_LINESATROOT 在根/子结点之间绘制连线 
TVS_HASBUTTONS 在每一个结点前添加一个按钮，用于表示当前结点是否已被展开 
TVS_EDITLABELS 结点的显示字符可以被编辑 
TVS_SHOWSELALWAYS 在失去焦点时也显示当前选中的结点 
TVS_DISABLEDRAGDROP 不允许Drag/Drop 
TVS_NOTOOLTIPS 不使用ToolTip显示结点的显示字符

long lStyle =GetWindowLong (this ->GetTreeCtrl ().m_hWnd ,GWL_STYLE );  

  lStyle |=TVS_HASBUTTONS |TVS_HASLINES |TVS_LINESATROOT |TVS_EDITLABELS ;  

  SetWindowLong (this ->GetTreeCtrl ().m_hWnd ,GWL_STYLE ,lStyle );  


在树形控件中每一个结点都有一个句柄（HTREEITEM），同时添加结点时必须提供的参数是该结点的父结点句柄，（其中根Root结点只有一个，既不可以添加也不可以删除）利用
HTREEITEM InsertItem( LPCTSTR lpszItem, HTREEITEM hParent = TVI_ROOT, HTREEITEM hInsertAfter = TVI_LAST );
可以添加一个结点，pszItem为显示的字符，hParent代表父结点的句柄，当前添加的结点会排在hInsertAfter表示的结点的后面，返回值为当前创建的结点的句柄。下面的代码会建立一个如下形式的树形结构：

+--- Parent1
    +--- Child1_1
    +--- Child1_2
    +--- Child1_3
+--- Parent2
+--- Parent3

/*假设m_tree为一个CTreeCtrl对象，而且该窗口已经创建*/
HTREEITEM hItem,hSubItem;
hItem = m_tree.InsertItem("Parent1",TVI_ROOT);在根结点上添加Parent1
hSubItem = m_tree.InsertItem("Child1_1",hItem);//在Parent1上添加一个子结点
hSubItem = m_tree.InsertItem("Child1_2",hItem,hSubItem);//在Parent1上添加一个子结点，排在Child1_1后面
hSubItem = m_tree.InsertItem("Child1_3",hItem,hSubItem);

hItem = m_tree.InsertItem("Parent2",TVI_ROOT,hItem);    
hItem = m_tree.InsertItem("Parent3",TVI_ROOT,hItem);   

如果你希望在每个结点前添加一个小图标，就必需先调用
CImageList* SetImageList( CImageList * pImageList, int nImageListType );
指明当前所使用的ImageList，nImageListType为TVSIL_NORMAL。在调用完成后控件中使用图片以设置的ImageList中图片为准。然后调用
HTREEITEM InsertItem( LPCTSTR lpszItem, int nImage, int nSelectedImage, HTREEITEM hParent = TVI_ROOT, HTREEITEM hInsertAfter = TVI_LAST);
添加结点，nImage为结点没被选中时所使用图片序号，nSelectedImage为结点被选中时所使用图片序号。下面的代码演示了ImageList的设置。

/*m_list 为CImageList对象
IDB_TREE 为16*(16*4)的位图，每个图片为16*16共4个图标*/
m_list.Create(IDB_TREE,16,4,RGB(0,0,0));
m_tree.SetImageList(&m_list,TVSIL_NORMAL);
m_tree.InsertItem("Parent1",0,1);//添加，选中时显示图标1，未选中时显示图标0


此外CTreeCtrl还提供了一些函数用于得到/修改控件的状态。 
HTREEITEM GetSelectedItem( );将返回当前选中的结点的句柄。
BOOL SelectItem( HTREEITEM hItem );将选中指明结点。 
BOOL GetItemImage( HTREEITEM hItem, int& nImage, int& nSelectedImage ) / BOOL SetItemImage( HTREEITEM hItem, int nImage, int nSelectedImage )用于得到/修改某结点所使用图标索引。 
CString GetItemText( HTREEITEM hItem ) /BOOL SetItemText( HTREEITEM hItem, LPCTSTR lpszItem );用于得到/修改某一结点的显示字符。 
BOOL DeleteItem( HTREEITEM hItem );用于删除某一结点，
BOOL DeleteAllItems( );将删除所有结点。

此外如果想遍历树可以使用下面的函数： 
HTREEITEM GetRootItem( );得到根结点。 
HTREEITEM GetChildItem( HTREEITEM hItem );得到子结点。 
HTREEITEM GetPrevSiblingItem/GetNextSiblingItem( HTREEITEM hItem );得到指明结点的上/下一个兄弟结点。 
HTREEITEM GetParentItem( HTREEITEM hItem );得到父结点。

树形控件的消息映射使用ON_NOTIFY宏，形式如同：ON_NOTIFY( wNotifyCode, id, memberFxn )，wNotifyCode为通知代码，id为产生该消息的窗口ID，memberFxn为处理函数，函数的原型如同void OnXXXTree(NMHDR* pNMHDR, LRESULT* pResult)，其中pNMHDR为一数据结构，在具体使用时需要转换成其他类型的结构。对于树形控件可能取值和对应的数据结构为：

TVN_SELCHANGED 在所选中的结点发生改变后发送，所用结构：NMTREEVIEW 
TVN_ITEMEXPANDED 在某结点被展开后发送，所用结构：NMTREEVIEW 
TVN_BEGINLABELEDIT 在开始编辑结点字符时发送，所用结构：NMTVDISPINFO 
TVN_ENDLABELEDIT 在结束编辑结点字符时发送，所用结构：NMTVDISPINFO 
TVN_GETDISPINFO 在需要得到某结点信息时发送，（如得到结点的显示字符）所用结构：NMTVDISPINFO 
关于ON_NOTIFY有很多内容，将在以后的内容中进行详细讲解。

关于动态提供结点所显示的字符：首先你在添加结点时需要指明lpszItem参数为：LPSTR_TEXTCALLBACK。在控件显示该结点时会通过发送TVN_GETDISPINFO来取得所需要的字符，在处理该消息时先将参数pNMHDR转换为LPNMTVDISPINFO，然后填充其中item.pszText。但是我们通过什么来知道该结点所对应的信息呢，我的做法是在添加结点后设置其lParam参数，然后在提供信息时利用该参数来查找所对应的信息。下面的代码说明了这种方法：

char szOut[8][3]={"No.1","No.2","No.3"};

//添加结点
HTREEITEM hItem = m_tree.InsertItem(LPSTR_TEXTCALLBACK,...)
m_tree.SetItemData(hItem, 0 );
hItem = m_tree.InsertItem(LPSTR_TEXTCALLBACK,...)
m_tree.SetItemData(hItem, 1 );
//处理消息
void CParentWnd::OnGetDispInfoTree(NMHDR* pNMHDR, LRESULT* pResult)
{
TV_DISPINFO* pTVDI = (TV_DISPINFO*)pNMHDR;
pTVDI->item.pszText=szOut[pTVDI->item.lParam];//通过lParam得到需要显示的字符在数组中的位置
*pResult = 0;
}

关于编辑结点的显示字符：首先需要设置树形控件的TVS_EDITLABELS风格，在开始编辑时该控件将会发送TVN_BEGINLABELEDIT，你可以通过在处理函数中返回TRUE来取消接下来的编辑，在编辑完成后会发送TVN_ENDLABELEDIT，在处理该消息时需要将参数pNMHDR转换为LPNMTVDISPINFO，然后通过其中的item.pszText得到编辑后的字符，并重置显示字符。如果编辑在中途中取消该变量为NULL。下面的代码说明如何处理这些消息：

//处理消息 TVN_BEGINLABELEDIT
void CParentWnd::OnBeginEditTree(NMHDR* pNMHDR, LRESULT* pResult)
{
TV_DISPINFO* pTVDI = (TV_DISPINFO*)pNMHDR;
if(pTVDI->item.lParam==0);//判断是否取消该操作
*pResult = 1;
else
*pResult = 0;
}
//处理消息 TVN_BEGINLABELEDIT
void CParentWnd::OnBeginEditTree(NMHDR* pNMHDR, LRESULT* pResult)
{
TV_DISPINFO* pTVDI = (TV_DISPINFO*)pNMHDR;
if(pTVDI->item.pszText==NULL);//判断是否已经取消取消编辑
m_tree.SetItemText(pTVDI->item.hItem,pTVDI->pszText);//重置显示字符
*pResult = 0;
}


 

（一）树控制的主要功能

树控制和视（Tree Control&View)主要用来显示具有一定层次结构的数据项，如资源管理器中的磁盘目录等，以供用户在其中进行各种选择。树控制中的每个数据项包括数据项名称的文本字符串和用于表示该数据项的图像，每个数据项下面均可包含各种子项，整个结构就象目录树一样。对于包含各种子项的数据项，可通过鼠标双击来展开或合拢，这可以通过控制树的不同风格来实现树控制的不同显示形态。这些风格主要包括：

TVS_HASLINES表示用连线来连接父项和它下面的各个子项，这可以使树的显示层次结构更加清晰，但在无父项的各子项之间并没有连线；

TVS_LINESATROOT表示在无父项的各子项即根下面的各子项之间存在连线；

TVS_HASBUTTONS表示在带有子项的父项前面增加一个带“＋”或“－”的按钮，这使得用户也可以通过单击这个小按钮来实现子项的展开和合拢，当存在子项时，按钮的初始状态为“＋”，当子项被展开时，按小按钮由“＋”变为“－”号，当子项合拢时，小按钮由“－”变为“＋”号，这一风格同样对于根项无效，如果需要可通过组合TVS_LINESATROOT风格来实现；

TVS_EDITLABELS表示允许让用户单击具有输入焦点的数据项来修改其名称。

对于树控制，MFC中也以两种形式来封装，即树控制（CTREECTRL）和树视（CTREEVIEW），来满足用户的不同需求，对于一般要求的用户如在对话框中应用，使用树控制比较方便，而对于具有较高要求的用户，在使用树视时还具有视窗口的各种方便特性，可以更好地满足文档／视结构的要求。当在窗口中使用树视时，树视会占满两个窗口的客户区域并自动随窗口的框架结构的调整而调整，并能够很好地处理诸如菜单、加速键和工具条中的各种命令消息。在使用树视时只要利用其成员函数CtreeView取得其一个引用，就可以象树控制一样方便地应用：CtreeCtrl &treeCtrl = GetTreeCtrl()。

（二）树控制的对象结构

1、树控制的建立方法

CtreeCtrl＆treeCtrl 建立树控制对象结构

Create 建立树控制并绑定对象

树控制CTreeCtrl::Create的调用格式如下：

BOOL Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID );

其中参数dwStyle用来确定树控制的类型；rect用来确定树控制的大小和位置；pParentWnd用来确定树控制的父窗口，通用是一个对话框并且不能为NULL；nID用来确定树控制的标识。树控制的风格可以是下列值的组合：

TVS_HASLINES 表示树控制在各子项之间存在连线；

TVS_LINESATROOT 表示树控制在根项之间存在连线；

TVS_HASBUTTONS 表示树控制视在父项左侧存在展开合拢控制按钮；

TVS_EDITLABELS 表示可以控制鼠标单击修改树项的名称；

TVS_SHOWSELALWAYS 表示选中项即使在窗口失去输入焦点时仍然保持选中状态；

TVS_DISABLEDRAGDROP表示禁止树控制发送TVN_BEGINDRAG消息

2、树控制的属性类

树控制属性类包括取得树控制中项数GetCount、取得树控制中项相对于父项的偏移值GetIndent、取得树控制图像列表控制句柄GetImageList、设置树控制图像列表控制句柄SetImageList、取得匹配下一个树项GetNextItem、判断给定树项是否包含子项ItemHasChildren、取得树项子项GetChildItem、取得下一个同属树项GetNextSiblingItem、取得前一个同属树项GetPrevSiblingItem、取得父树项GetParentItem、取得第一个可视树项GetFirstVisibleItem、取得下一个可视树项GetNextVisible Item、取得前一个可视的树项GetPrevVisibleItem、取得被选中的树项GetSelectedItem、取得根树项GetRootItem、取得树项的属性GetItem、设置树项的属性SetItem、取得树项的状态GetItemState、设置树项的状态SetItemState、取得与树项关联图像GetItemImage、设置与树项关联图像SetItemImage、取得树项文本GetItemText、设置树项文本SetItemText和取得树项编辑控制句柄GetEditControl等。

3、树控制的操作方法

树控制的操作方法包括插入一个树项InsertItem、删除一个树项DeleteItem、删除所有树项DeleteAllItems、展开或合拢树项的子项Expand、选中特定树项SelectItem、选择一个树项作为第一个可视树项SelectSetFirstVisible、编辑一个可视的树项EditLabel和排序给定父树项的子树项SortChildren等。

（三）树控制的数据结构

在使用树控制时需要了解两个个非常重要的数据结构TV_ITEM和TV_INSERTSTRUCT，前一个数据结构是用来表示树控制的树项信息，后一个数据结构是用来定义将树项增加到数据控制中所需要的数据内容。另外，还需要NM_TREEVIEW、TV_DISPINFO和TV_HITTESTINFO三个数据结构，这几个数据结构的定义方法如下：

①基本数据项结构

typedef struct _TV_ITEM {

UINT mask; //结构成员有效性屏蔽位

HTREEITEM hItem; //数据项控制句柄

UINT state; //数据项状态

UINT stateMask; //状态有效性屏蔽位

LPSTR pszText; //数据项名称字符串

int cchTextMax; //数据项名称的最大长度

int iImage; //数据项图标索引号

int iSelectedImage;//选中数据项图标索引号

int cChildren; //子项标识

LPARAM lParam; //程序定义的32位数据

} TV_ITEM, FAR *LPTV_ITEM;

②插入树项结构

typedef struct _TV_INSER TSTRUCT {

HTREEITEM hParent; //父项控制句柄

HTREEITEM hInsertAfter; //插入树项的位置

TV_ITEM item; //数据项的结构

} TV_INSERTSTRUCT, FAR *LPTV_INSERTSTRUCT;

其中插入的位置如果是TVI_FIRST 或TVI_LAST ，则分别插入到树控制的最前面或最后面，如果是TVI_SORT ，则插入的树项自动插入到合适的位置。

③树控制通知消息结构

typedef struct _NM_TREEVIEW {

NMHDR hdr; //通知消息句柄

UINT action; //通知消息标志

TV_ITEM itemOld; //原来的数据结构

TV_ITEM itemNew; //新的数据结构

POINT ptDrag; //拖动指针

} NM_TREEVIEW;

④取得或设置数据结构

typedef struct _TV_DISPINFO { tvdi

NMHDR hdr; //通知消息控制句柄

TV_ITEM item; //数据项结构

} TV_DISPINFO;

⑤指针测试数据结构

typedef struct _TVHITTESTINFO { tvhtst

POINT pt; //客户区域屏幕坐标指针

UINT flags; //存放测试结果的变量

HTREEITEM hItem; //测试的数据项结构

} TV_HITTESTINFO, FAR *LPTV_HITTESTINFO;

其中flags测试结果可以是如下值：

TVHT_ABOVE 在客户区域上面

TVHT_BELOW 在客户区域下面

TVHT_NOWHERE 在客户区域中并在最后一项下面

TVHT_ONITEM 在与树项关联的位图或标签内

TVHT_ONITEMBUTTON 在与树项关联的按钮上

TVHT_ONITEMICON 在与树项关联的位图上

TVHT_ONITEMINDENT 在与树项关联的联线上

TVHT_ONITEMLABEL 在与树项关联的标签上

TVHT_ONITEMRIGHT 在树项的右侧区域中

TVHT_ONITEMSTATEICON 在用户定义的状态图标上

TVHT_TOLEFT 在客户区域的左侧

TVHT_TORIGHT 在客户区域的右侧

（四）树控制的应用技巧示例

这里仍以基于对话框演示实例来具体介绍树控制及其和图像列表相结构的应用技巧：

通过“FILE->NEW->PROJECTS->MFC AppWizard(EXE)”建立名为VCTREE的工程，在建立过程中选择基于对话框（Dialog based）的应用；将对话框中的默认控件删除，并将所有对话框属性中的Language域设置为Chinese(P.R.C.),以使应用程序支持中文；建立两个图标IDI_PM和IDI_CJ，用来表示图标的选中和非选中状态，对于每个图标都应建立32X32和16X16两种大小，以保证程序的需要；在对话框窗口中添加树控制对象（TREE CONTROL）,并设置五个按钮“增加｜删除｜查看｜排序｜关闭”，其对应标识分别如下：


--------------------------------------------------------------------------------

 


控制名称 标题名称 标识符号


--------------------------------------------------------------------------------

 


树控制 IDC_TREECTRL

按钮 增 加 IDC_ADD

删 除 IDC_DEL

查 看 IDC_VIEW

排 序 IDC_SORT

关 闭 IDOK


--------------------------------------------------------------------------------

 


5、选中树控制控件，选择“VIEW->ClassWizard->Memory Variables。 骺刂艻DC_TREECTRL 引入成员变量，其变量类型为：

变量名 种类 变量类型

m_TreeCtrl Control CTreeCtrl

同时利?ldquo;MESSAGES MAP”为各命令按钮增加控制功能函数。

6、然后在代码文件VCTREEDlg.CPP中分别加入如下控制代码：

（1）在文件开始处增加图像列表定义

CImageList Cil1,Cil2;//大小图标像列表

（2）在初始化文件开始处增加代码

BOOL CVCTREEDlg::OnInitDialog()

{ CDialog::OnInitDialog();

......//原来其它代码

// TODO: Add extra initialization here

// 此处开始增加代码

CVCTREEApp *pApp=(CVCTREEApp *)AfxGetApp();//创建图象列表

Cil1.Create(16,16,ILC_COLOR,2,2);

Cil1.Add(pApp->LoadIcon(IDI_PM));

Cil1.Add(pApp->LoadIcon(IDI_CJ));

m_TreeCtrl.SetImageList(&Cil1,TVSIL_NORMAL); //设置图象列表

DWORD dwStyles=GetWindowLong(m_TreeCtrl.m_hWnd,GWL_STYLE);//获取树控制原风格

dwStyles|=TVS_EDITLABELS|TVS_HASBUTTONS|TVS_HASLINES|TVS_LINESATROOT;

SetWindowLong(m_TreeCtrl.m_hWnd,GWL_STYLE,dwStyles);//设置风格

char * CJ[4]={"玉溪卷烟厂","云南卷烟厂","沈阳卷烟厂","成都卷烟厂"};//根数据名称

char * PM[4][5]={

{"红梅一","红梅二","红梅三","红梅四","红梅五"},//产品数据项

{"白梅一","白梅二","白梅三","白梅四","白梅五"},

{"绿梅一","绿梅二","绿梅三","绿梅四","绿梅五"},

{"青梅一","青梅二","青梅三","青梅四","青梅五"}};

int i,j;

HTREEITEM hRoot,hCur;//树控制项目句柄

TV_INSERTSTRUCT TCItem;//插入数据项数据结构

TCItem.hParent=TVI_ROOT;//增加根项

TCItem.hInsertAfter=TVI_LAST;//在最后项之后

TCItem.item.mask=TVIF_TEXT|TVIF_PARAM|TVIF_IMAGE|TVIF_SELECTEDIMAGE;//设屏蔽

TCItem.item.pszText="数据选择";

TCItem.item.lParam=0;//序号

TCItem.item.iImage=0;//正常图标

TCItem.item.iSelectedImage=1;//选中时图标

hRoot=m_TreeCtrl.InsertItem(&TCItem);//返回根项句柄

for(i=0;i<4;i++){//增加各厂家

TCItem.hParent=hRoot;

TCItem.item.pszText=CJ[i];

TCItem.item.lParam=(i+1)*10;//子项序号

hCur=m_TreeCtrl.InsertItem(&TCItem);

for(j=0;j<5;j++){//增加各产品

TCItem.hParent=hCur;

TCItem.item.pszText=PM[i][j];

TCItem.item.lParam=(i+1)*10+(j+1);//子项序号

m_TreeCtrl.InsertItem(&TCItem);

}

m_TreeCtrl.Expand(hCur,TVE_EXPAND);//展开树

}

m_TreeCtrl.Expand(hRoot,TVE_EXPAND);//展开上一级树

return TRUE; // return TRUE unless you set the focus to a control

}

（3）增加树项功能的实现

在增加树项功能时，除了需要定义和设置插入树项的数据结构之外，还需要注意的是新增树项的名称初始时均为“新增数据”，增加后允许用户给数据项设置自定义名称。在编程时应特别注意m_TreeCtrl.EditLabel(hInsert);后面不能跟任何其它程序命令，否则这条编辑指令无效。

void CVCTREEDlg::OnAdd()

{ //增加子项功能函数

HTREEITEM hSel=m_TreeCtrl.GetSelectedItem();//取得选择项句柄

if(hSel==NULL) return;//无任何选项则返回

static int nAddNo=100;//编号大于100为新增数据

TV_INSERTSTRUCT TCItem;//定义插入项数据结构

TCItem.hParent=hSel; //设置父项句柄

TCItem.hInsertAfter=TVI_LAST;//在最后增加

TCItem.item.mask=TVIF_TEXT|TVIF_PARAM|TVIF_IMAGE|TVIF_SELECTEDIMAGE;//设屏蔽

TCItem.item.pszText="新增数据";

TCItem.item.lParam=nAddNo++;//索引号增加

TCItem.item.iImage=0;//正常图标

TCItem.item.iSelectedImage=1;//选中时图标

HTREEITEM hInsert=m_TreeCtrl.InsertItem(&TCItem);//增加

m_TreeCtrl.Expand(hSel,TVE_EXPAND);

m_TreeCtrl.EditLabel(hInsert);//修改增加的数据

}

（4）删除树项功能的实现

在实现删除功能时，应对存在子项的树项进行提示，以警告用户是否连同其子项一起删除。

void CVCTREEDlg::OnDel()

{ //删除子项功能函数

HTREEITEM hSel=m_TreeCtrl.GetSelectedItem();//取得选项句柄;

if(hSel==NULL) return;//无任何选项则返回

if(m_TreeCtrl.ItemHasChildren(hSel))//判断是否有子项

if(MessageBox("厂家下存在品名,一同删除?","警告",MB_YESNO)==IDNO) return;

m_TreeCtrl.DeleteItem(hSel);

}

（5）排序功能的实现

排序功能是对所选中的树项的所有子项按字符中顺序进行排序，如果想要按照其它规则进行排序，应利用SortChildrenItemBC()函数进行自行开发排序程序，这个自行开发的函数与列表控制中实现的函数基本相同，可兴趣的读可以试验。

void CVCTREEDlg::OnSort()

{ //排序子项功能函数

HTREEITEM hSel=m_TreeCtrl.GetSelectedItem();//取得选项句柄;

if(hSel==NULL) return;//无任何选项则返回

m_TreeCtrl.SortChildren(hSel);

}

（6）查看功能的实现

查看功能用来查看选中树项的有关信息，函数中中显示了树项的文本名称和标识号，可以将这两个信息作为查找关键字，来查看其它更详细的信息。

void CVCTREEDlg::OnView()

{ //查看选中项功能函数

HTREEITEM hSel=m_TreeCtrl.GetSelectedItem();//取得选项句柄;

if(hSel==NULL) return;//无任何选项则返回

CString cText=m_TreeCtrl.GetItemText(hSel);//取得数据项名

LONG IDs=m_TreeCtrl.GetItemData(hSel);//取得数据项序号

char temp[100];

wsprintf(temp,"厂家:%s 编号:%05d",cText,IDs);

MessageBox(temp,"选择信息");

}

（7）修改功能的实现

如果不进行其它处理，当修改树项的文本名称后，就会发现其未被修改，这是因为程序中没有对修改结果进行保存处理，这就要利用TV_DISPINFO结构和SetItemText函数对TVN＿ENDLABELEDIT进行处理，这样就可以正确地实现修改功能。

void CVCTREEDlg::OnEndlabeleditTree(NMHDR* pNMHDR, LRESULT* pResult)

{ TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;

// TODO: Add your control notification handler code here

if(pTVDispInfo->item.pszText==0) return;//用户取消修改操作

m_TreeCtrl.SetItemText(pTVDispInfo->item.hItem,

pTVDispInfo->item.pszText);//设置新数据

*pResult = 0;

}

7、树视的演练技巧

树视的应用技巧在使用树视时，其方法与树控制基本相同，只不过树视是在窗口中来实现的而树控制是在对话框中实现，树视的各种功能是通过菜单来实现的而树控制是通过按钮等方式来实现的，树控制需要在对话框中创建树控制控件而树视直接占据整个窗口，在设计过程中只要将按钮和树控制设计过程变为菜单设计，并注意在功能函数是在类向导中是通过菜单命令来操作，同时在每个功能函数前面增加取得列表视引用的命令（CTreeCtrl& TreeCtrl = GetTreeCtrl()），而其余数据结构和代码均不需要修改，实现起来比较容易。笔者实现的树控制和视程序的运行结果如下：

   


function showMiniAd(){ var vn = "tinfo"; var dataURL = "http://t.sohu.com/third/user.jsp?passport="+window._xpt+"&vn="+vn; new LinkFile(dataURL, { type: 'script', noCache: false, callBack: { variable: vn, onLoad: function(){ var data = eval("(" + vn + ")") if(data != null && data.status == 1){ var userLink = data.url; //var userLink = data.icon; $('miniAd').innerHTML = "我正在玩搜狐微博，快来“关注”我，了解我的最新动态吧。
"+userLink+""; $('miniAd').show(); } }, onFailure: function(){} }}); } showMiniAd(); 

 

