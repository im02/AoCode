
### 源码名称：Ctreectrl控件节点不同颜色 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
ctreectrl控件节点不同颜色
```
#### 下载：无
#### 使用方法：


``` c++
void CZvanRTSPClientDlg::OnNMCustomdrawTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTVCUSTOMDRAW pNMCD = reinterpret_cast<LPNMTVCUSTOMDRAW>(pNMHDR);
	switch (pNMCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;
	case CDDS_ITEMPREPAINT:
		// 这里做判断
		CString s = m_cameraTree.GetItemText((HTREEITEM)pNMCD->nmcd.dwItemSpec);
		if   (m_cameraTree.GetItemData((HTREEITEM)pNMCD->nmcd.dwItemSpec)) 
		{ 
			pNMCD->clrText = RGB(0, 128, 0); // 这样设置文字颜色
		} 
		else
		{
			pNMCD->clrText = RGB(210,   180,   140); // 这样设置文字颜色
		}
		
		break;
	}
	*pResult = 0;
}

```
