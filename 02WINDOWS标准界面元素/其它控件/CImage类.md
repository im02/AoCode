2009-04-19 19:04CImage类的成员可分为连接与创建、输入与输出、位图类型与参数，以及图形绘制与位图块传送等4类。
(1) 连接与创建
Attach 将一个DIB（或DDB）位图与CImage对象相连接
Detach 位图与CImage对象相分离
Create 创建一个DIB位图并将它与已有CImage对象相连接
Destroy 位图与Cimage对象相分离并删除
(2) 输入与输出
GetImporterFilterString 返回系统支持的输入文件格式类型及其描述
GetExporterFilterString 返回系统支持的输出文件格式类型及其描述
Load 读入指定图像文件中的图像
LoadFromResource 读入指定资源文件中的图像
Save 按指定类型保存图像，文件名中不能省略后缀
IsNull 判别源图像文件是否已经读入
(3) 位图类型与参数
GetWidth 返回当前图像的宽度（以像素为单位）
GetHeight 返回当前图像的高度
GetBPP 返回当前图像的每像素位数
GetBits 返回当前图像像素数据的指针
GetPitch 返回相邻两行像素首地址之间的间隔
GetPixelAddress 返回指定像素数据存放位置的存储地址
GetMaxColorTableEntries返回调色板单元数
IsDibSection 确定位图是否为DIB位图
IsIndexed 判别位图中是否有调色板
IsTransparencySupported判别应用程序是否支持透明位图
AlphaBlend 是否支持透明或半透明显示的状态
(4) 图形绘绘与位图块传送
GetDC 返回当前位图的设备描述表
ReleaseDC 释放设备描述表
GetPixel 返回指定位置像素的颜色
SetPixel 设置指定位置像素的颜色
SetPixelIndexed 设置指定位置像素颜色的索引值
SetPixelRGB 设置指定位置像素的红绿蓝分量
GetColorTable 获取调色板颜色分量（红、绿、蓝）值
SetColorTable 设置调色板颜色分量（红、绿、蓝）值
SetTransparentColor 设置透明颜色的索引值，只能有一种颜色可为透明
Draw 从源矩形复制到目标矩形，支持伸缩、a融合与透明
BitBlt 位图的一个举行区域复制到另一位图上的制定区域
MaskBlt 为图块传送时须与指定的模板图像做逻辑操作
PlgBlt 为图的矩形区域到另一位图上平行四边形间的位块传输
StretchBlt 从源矩形复制到目标矩形，支持伸缩
TransparentBlt 位图块传送时复制带透明色的位图


FAQ:
1. 关于GetBits()函数的使用.对于一幅DIB图像来说，图像的左下角为像素数据的起点。通常我们会认为GetBits()函数返回的是图像左下角的 数据指针，但是CImage实现却不是这样。GetBits()返回的是图像左上角的像素数据指针。从这个角度上来讲，它的功能等效于函数 GetPixelAddress(0,0)调用。而且CImage的成员函数中坐标是以左上角为坐标原点来表示的。

   2. 关于SetPixelIndexed()函数的使用. 从函数名我们可以得知，该函数应该应用于非真彩色图像，特别是8位的图像。在测试该函数时，发现应用于8位BMP图像时，不管设置什么索引值，操作后的结 果显示索引值变成了0。这个现象确实让人匪夷所思。不知道是不是CImage类的该函数设计有问题。当然，如果将它不合理地应用于真彩色图像，操作的结果 还是黑色。

   3. 另外，CImage类中还有一些函数,譬如SetPixel()和SetPixelRGB()函数，理论上应该将它们应用于真彩色图像，但是如果强硬的将 它们应用于非真彩色图像(测试中将它们应用于8位BMP图像)，操作结果显示设置的值都会被“合理”的变成灰阶数据，比如说，如果我们设置RGB值为 (180,0,0),操作结果为调色板里的数据变为RGB(60,60,60). 
   
4. 直接调用CImage类的GetPixel和SetPixel效率是很低的， 如果要循环遍 历所有Pixels的话， 需要用GetPixelAddress直接取得像素数据的存储地址， 然后进行操作。这种方法有个问题： How to handle the PixelAddresses correctly with diffrent kinds of BPPs (Bits Per Pixel).
   For 24 BPP Images:
       for setting
   inline void setPixel(int x, int y, COLORREF &c)
   {
    BYTE *p = (BYTE*)imgage.GetPixelAddress(x, y);
    *p++ = GetRValue(c);
    *p++ = GetGValue(c);
    *p = GetBValue(c);
   }
  
  
   & for getting a pixel
  
   inline COLORREF getPixel(int x, int y) const
   { 
    return(*(COLORREF*)(image.GetPixelAddress(x, y))); 
   }

For 16 BPP Images:
   16-bit images would be easy to support, and they are much similar to 32-bit images.
   16-bit colour pixels are stored in 5-6-5 format (five bits for red, six bits for green, and five for blue).
  
   inline void setPixel(int x, int y, COLORREF &c)
   {
    WORD *p = (WORD*)imgage.GetPixelAddress(x, y);
    *p = ((GetRValue(c) >> 3) << 11) + ((GetGValue(c) >> 2) << 5) + (GetBValue(c) >> 3);
   }
  
   inline COLORREF getPixel(int x, int y) const
   {
    WORD *p = (WORD*)image.GetPixelAddress(x, y);
    return RGB(((*p & 0xF800) >> 8), ((*p & 0x07E0) >> 3), ((*p & 0x001F) << 3));
   }
 
