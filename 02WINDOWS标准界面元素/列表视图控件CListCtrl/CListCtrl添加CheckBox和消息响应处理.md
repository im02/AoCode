1、添加CheckBox.
CListCtrl控件添加一个CListCtrl型变量m_listctrl，利用m_listctrl.SetExtendedStyle(LVS_EX_CHECKBOXES);增加CheckBox. 
 
2、CListCtrl的CheckBox的消息响应。
(1)、头文件中添加响应函数：afx_msg void OnItemchangedlist(NMHDR* pNMHDR, LRESULT* pResult);
(2)、在 BEGIN_MESSAGE_MAP(ClassName, CDockablePane)和END_MESSAGE_MAP()间添加响应。ON_NOTIFY(LVN_ITEMCHANGED, 4, OnItemchangedlist)//4为CListCtrl的资源标识。
(3)、在CPP中添加响应函数：
void CClassName::OnItemchangedlist(NMHDR* pNMHDR, LRESULT* pResult)
{
    NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;   
    *pResult = 0;   
       
    if (pNMListView->uOldState == 0 && pNMListView->uNewState == 0)   // No change
        return; 
       
    BOOL bPrevState = (BOOL)(((pNMListView->uOldState &   LVIS_STATEIMAGEMASK)>>12)-1);   // Old check box state
    if(bPrevState < 0) // On startup there's no previous state    
        bPrevState = 0; // so assign as false (unchecked)   
       
    // New check box state   
    BOOL bChecked=(BOOL)(((pNMListView->uNewState & LVIS_STATEIMAGEMASK)>>12)-1);      
    if (bChecked < 0) // On non-checkbox notifications assume false   
        bChecked = 0;    
       
    if (bPrevState == bChecked) // No change in check box   
        return;   
       
    // bChecked表示当前item是否checked       
 CString str;
 str.Format(_T("%d"),pNMListView->iItem);
 AfxMessageBox(str);
 
 *pResult = 0; 
}