1. CInternetSession的简单使用
CInternetSession session;
CHttpFile *file = NULL;
CString strURL = " http://www.20abcd.com";
CString strHtml = "”;   //存放网页数据
 
try{
       file = (CHttpFile*)session.OpenURL(strURL);
}catch(CInternetException * m_pException){
       file = NULL;
       m_pException->m_dwError;
       m_pException->Delete();
       session.Close();
       MessageBox("CInternetException");
}
CString strLine;
if(file != NULL){
       while(file->ReadString(strLine) != NULL){
       strHtml += strLine;
       }
 
}else{
       MessageBox("fail");
}
 
session.Close();
file->Close();
delete file;
file = NULL;
 
2. CInternetSession的代理与超时使用
CInternetSession session;
CHttpFile *file = NULL;  
 
INTERNET_PROXY_INFO proxyinfo;
proxyinfo.dwAccessType = INTERNET_OPEN_TYPE_PROXY;
proxyinfo.lpszProxy ="211.104.243.73:8080";
proxyinfo.lpszProxyBypass = NULL;
session.SetOption(INTERNET_OPTION_PROXY,(LPVOID)&proxyinfo,
sizeof(INTERNET_PROXY_INFO));
 
session.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 5000);      // 5秒的连接超时
session.SetOption(INTERNET_OPTION_SEND_TIMEOUT, 1000);           // 1秒的发送超时
session.SetOption(INTERNET_OPTION_RECEIVE_TIMEOUT, 7000);        // 7秒的接收超时
session.SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT, 1000);     // 1秒的发送超时
session.SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT, 7000);       // 7秒的接收超时
session.SetOption(INTERNET_OPTION_CONNECT_RETRIES, 1);          // 1次重试
 
try{
       file = (CHttpFile*)session.OpenURL("http://www.163.com",1,
INTERNET_FLAG_TRANSFER_ASCII|INTERNET_FLAG_RELOAD|INTERNET_FLAG_DONT_CACHE);
}catch(CInternetException * m_pException){
       file = NULL;
       m_pException->m_dwError;
       m_pException->Delete();
       session.Close();
       MessageBox("CInternetException");
       return;
}
CString strLine;
if(file != NULL){
       while(file->ReadString(strLine) != NULL){
              MessageBox(strLine);
       }
}else{
       MessageBox("fail");
}
file->Close();
session.Close();
 
3. CInternetSession的POST使用
CInternetSession m_InetSession(_T("session"),
       0,
       INTERNET_OPEN_TYPE_PRECONFIG,
       NULL,
       NULL,
       INTERNET_FLAG_DONT_CACHE);     //设置不缓冲
CHttpConnection* pServer = NULL;
CHttpFile* pFile = NULL;
CString strHtml = "";
CString strRequest = "name=123&pwd=321\r\n"; //POST过去的数据
CString strHeaders = "Accept: */*\r\nReferer: http://www.goodwaiter.com/\r\nAccept-Language: zh-cn\r\nContent-Type: application/x-www-form-urlencoded\r\nAccept-Encoding: gzip, deflate\r\nUser-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; Maxthon; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
 
try{
       INTERNET_PORT nPort; //端口
       nPort=80;
       pServer = m_InetSession.GetHttpConnection("www.goodwaiter.com", nPort);
       pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST,"/");
       pFile->AddRequestHeaders(strHeaders);
 
       pFile->SendRequestEx(strRequest.GetLength());
       pFile->WriteString(strRequest); //重要-->m_Request 中有"name=aaa&name2=BBB&..."
       pFile->EndRequest();
       DWORD dwRet;
       pFile->QueryInfoStatusCode(dwRet);
 
       if (dwRet == HTTP_STATUS_OK){
              CString strLine;
              while ((nRead = pFile->ReadString(strLine))>0)
              {
                     strHtml += strLine;
              }
       }    
       delete pFile;
       delete pServer;
}
catch (CInternetException* e){
       e->m_dwContext;
}