
### 源码名称：VC模拟鼠标的两种方式(SendMessage、mouse_event) 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
VC模拟鼠标的两种方式(SendMessage、mouse_event)
```
#### 下载：无
#### 使用方法：


``` c++
鼠标模拟的常用方案，包括发送鼠标事件消息和使用mouse_event系统函数，发送鼠标消息的例子如下：

pWnd->SendMessage(WM_RBUTTONDOWN,0,(y<<16)|x);
这种方法不需要窗体在前端，甚至最小化也可以使用，但是此方法并不是在所有场合有效，特别是对于不响应鼠标消息的程序更是如此。在这种情况下，可以尝试使用mouse_event函数。
首先给出mouse_event函数的原型：

VOID mouse_event(      
 
    DWORD <em>dwFlags</em>,
    DWORD <em>dx</em>,
    DWORD <em>dy</em>,
    DWORD <em>dwData</em>,
    ULONG_PTR <em>dwExtraInfo</em>
);
mouse_event有五个参数，第 一个为选项标志，为MOUSEEVENTF_LEFTDOWN时表示左键按下为MOUSEEVENTF_LEFTUP表示左键松开，向系统发送相应消息；第二、三个参数分别表示模拟鼠标对应x，y的位置，需要注意测是该参数对应的是屏幕坐标；第四、五个参数并不重要，一般也可设为0,0。若要得到Keybd_event和mouse_event函数的更详细的用法，可以查阅msdn。
mouse_even只能够发送前台消息，即仅对当前激活的窗体有效。t最好配合SetCursorPos(x,y)函数一起使用，首先调用SetCursorPos函数设置鼠标位置到需要模拟鼠标操作的位置，然后调用mouse_event模拟鼠标操作，下面是关于mouse_event的示例代码：

POINT lpPoint;
GetCursorPos(&lpPoint);
SetCursorPos(lpPoint.x, lpPoint.y);
mouse_event(MOUSEEVENTF_LEFTDOWN,0,0,0,0);
mouse_event(MOUSEEVENTF_LEFTUP,0,0,0,0);
示例代码表示鼠标的双击，若要表示单击，用两个mouse_event即可(一次放下，一次松开)。
上面说明了模拟鼠标操作的两种常用方法，下面再给出一个综合实例进行阐述说明：


//在发送按键消息前需要设置下鼠标位置，扫雷程序似乎是根据鼠标位置
//确定点击的方块的，而不是鼠标消息的参数
//所以PostMessage在这里也不可以使用
::SetCursorPos(x,y);
//判断是否是雷，不是雷才执行鼠标点击动作
if(MineInf[acol*row+arow]==1){
    /*::mouse_event(MOUSEEVENTF_RIGHTDOWN,x,y,0,0);
    ::mouse_event(MOUSEEVENTF_RIGHTUP,x,y,0,0);  */
    pWnd->SendMessage(WM_RBUTTONDOWN,0,(y<<16)|x);
    pWnd->SendMessage(WM_RBUTTONUP,0,(y<<16)|x);
}else{
    /*::mouse_event(MOUSEEVENTF_LEFTDOWN,x,y,0,0);
    ::mouse_event(MOUSEEVENTF_LEFTUP,x,y,0,0);*/            
    pWnd->SendMessage(WM_LBUTTONDOWN,0,(y<<16)|x);
    pWnd->SendMessage(WM_LBUTTONUP,0,(y<<16)|x);
}  
```
