1.   宽字符处理函数函数与普通函数对照表 

          字符分类: 

            宽字符函数       普通C函数           描述 
            iswalnum()       isalnum()           测试字符是否为数字或字母 
            iswalpha()       isalpha()           测试字符是否是字母 
            iswcntrl()       iscntrl()           测试字符是否是控制符 
            iswdigit()       isdigit()           测试字符是否为数字 
            iswgraph()       isgraph()           测试字符是否是可见字符 
            iswlower()       islower()           测试字符是否是小写字符 
            iswprint()       isprint()           测试字符是否是可打印字符 
            iswpunct()       ispunct()           测试字符是否是标点符号 
            iswspace()       isspace()           测试字符是否是空白符号 
            iswupper()       isupper()           测试字符是否是大写字符 
            iswxdigit()     isxdigit()         测试字符是否是十六进制的数字 
    
          大小写转换: 

            宽字符函数       普通C函数           描述 
            towlower()       tolower()           把字符转换为小写 
            towupper()       toupper()           把字符转换为大写 
    
          字符比较: 

            宽字符函数       普通C函数           描述 
            wcscoll()         strcoll()           比较字符串 
    
          日期和时间转换: 

            宽字符函数                                 描述 
            strftime()                                 根据指定的字符串格式和locale设置格式 
                                                                化日期和时间 
            wcsftime()                                 根据指定的字符串格式和locale设置格式 
                                                                化日期和时间,   并返回宽字符串 
            strptime()                                 根据指定格式把字符串转换为时间值, 
                                                                  是strftime的反过程 
          打印和扫描字符串: 

            宽字符函数                                 描述 
            fprintf()/fwprintf()             使用vararg参量的格式化输出 
            fscanf()/fwscanf()                 格式化读入 
            printf()                                     使用vararg参量的格式化输出到标准输出 
            scanf()                                       从标准输入的格式化读入 
            sprintf()/swprintf()             根据vararg参量表格式化成字符串 
            sscanf()                                     以字符串作格式化读入 
            vfprintf()/vfwprintf()         使用stdarg参量表格式化输出到文件 
            vprintf()                                   使用stdarg参量表格式化输出到标准输出 
            vsprintf()/vswprintf()         格式化stdarg参量表并写到字符串 
    
          数字转换: 

            宽字符函数       普通C函数           描述 
            wcstod()           strtod()             把宽字符的初始部分转换为双精度浮点数 
            wcstol()           strtol()             把宽字符的初始部分转换为长整数 
            wcstoul()         strtoul()           把宽字符的初始部分转换为无符号长整数 
    
          多字节字符和宽字符转换及操作: 

            宽字符函数                                 描述 
            mblen()                                       根据locale的设置确定字符的字节数 
            mbstowcs()                                 把多字节字符串转换为宽字符串 
            mbtowc()/btowc()                     把多字节字符转换为宽字符 
            wcstombs()                                 把宽字符串转换为多字节字符串 
            wctomb()/wctob()                     把宽字符转换为多字节字符 
          输入和输出: 
  
            宽字符函数       普通C函数           描述 
            fgetwc()           fgetc()               从流中读入一个字符并转换为宽字符 
            fgetws()           fgets()               从流中读入一个字符串并转换为宽字符串 
            fputwc()           fputc()               把宽字符转换为多字节字符并且输出到标 
                                                                  准输出 
            fputws()           fputs()               把宽字符串转换为多字节字符并且输出到 
                                                                  标准输出串 
            getwc()             getc()                 从标准输入中读取字符,   并且转换为宽字 
                                                                  符 
            getwchar()       getchar()           从标准输入中读取字符,   并且转换为宽字 
                                                                  符 
            None                   gets()                 使用fgetws() 
            putwc()             putc()                 把宽字符转换成多字节字符并且写到标准 
                                                                  输出 
            putwchar()       getchar()           把宽字符转换成多字节字符并且写到标准 
                                                                  输出 
            None                   puts()                 使用fputws() 
            ungetwc()         ungetc()             把一个宽字符放回到输入流中 
    
          字符串操作: 

            宽字符函数                 普通C函数         描述 
            wcscat()                     strcat()           把一个字符串接到另一个字符串的尾部 
            wcsncat()                   strncat()         类似于wcscat(),   而且指定粘接字符串的 
                                                                          粘接长度. 
            wcschr()                     strchr()           查找子字符串的第一个位置 
            wcsrchr()                   strrchr()         从尾部开始查找子字符串出现的第一个位 
                                                                          置 
            wcspbrk()                   strpbrk()         从一字符字符串中查找另一字符串中任何 
                                                                          一个字符第一次出现的位置 
            wcswcs()/wcsstr()   strchr()           在一字符串中查找另一字符串第一次出现 
                                                                          的位置 
            wcscspn()                   strcspn()         返回不包含第二个字符串的的初始数目 
            wcsspn()                     strspn()           返回包含第二个字符串的初始数目 
            wcscpy()                     strcpy()           拷贝字符串 
            wcsncpy()                   strncpy()         类似于wcscpy(),   同时指定拷贝的数目 
            wcscmp()                     strcmp()           比较两个宽字符串 
            wcsncmp()                   strncmp()         类似于wcscmp(),   还要指定比较字符字符 
                                                                          串的数目 
            wcslen()                     strlen()           获得宽字符串的数目 
    
            wcstok()                     strtok()           根据标示符把宽字符串分解成一系列字符 
                                                                          串 
            wcswidth()                 None                   获得宽字符串的宽度 
            wcwidth()                   None                   获得宽字符的宽度 
    
          另外还有对应于memory操作的   wmemcpy(),   wmemchr(),   wmemcmp(),   wmemmove(), 
          wmemset(). 
 
 
 
对我有用[0] 丢个板砖[0] 引用 举报 管理 TOP  
 
artcpp
 
(迷失) 

等　级： 

 #9楼 得分：20回复于：2007-03-07 12:26:00转： 

        我们都知道windows操作系统有支持ANSI和支持Unicode的版本,也有都支持的 
.其中,windows98只能支持ANSI系统环境.windows2000可以支持Unicode和NSI.win 
dowsCE只支持Unicode.WindowsXP好象也是两个都支持.所以我们在开发多国语言的 
时候一般都选用windows2000.因为我们可以构件Unicode工程来支持多国语言的显 
示.(如果你不理解什么是Unicode可以参看 < <Windows核心编程> > 的第二章.(在ttp 
://www.china-pub.com/computers/common/info.asp?id=131提供第二章的免费下 
载).|完整的下载,我都忘记了|. 
        为了在控件中显示别国语言(如日文,阿拉伯文等)而不至于显示成????,或者说 
文字可以显示在控件上,但是你无法获取他们并将他们保存到我们常用的CString中 
,这就需要你创建Unicode工程,这样才能支持.如果你一定要在Ansi系统(win98),开 
发这样的东西.我暂时还没有搞出来。.呵呵,在 < <windows核心编程> > 里面说得很清 
楚,如果你要把这个东西加到里面,即便成功也会造成无法想象的不稳定.关于这点 
显示的问题,我在下面还有一些支持在98下显示的问题说明,介绍的是用DHTML控件 
的.下面我先说建立Unicode工程和用Rich   Edit   控件(用CRichEditCtrl类支持)显 
示多语言的问题. 
        创建Unicode工程只要按照下面步骤进行就可以了.(/***/注释段为摘抄) 
/*******************   //构件Unicode   Debug 
1.前提条件： 
      运行VC++的安装程序，选中MFC的UNICODE支持。 
2。VC++Build-> configurations...-> Add-> Configuration-> "Unicode   Debug "-> O 
K 
3.Project-> Setting-> C++-> Preprosesser   Definitions-> 加入UNICODE,_UNICODE 
,   去掉MBCS即可 
4。Project-> Setting-> lINK-> Category-> )Outputw-> 加入wWinMainCRTStartup。 

5选择 "Win32   Unicode   debug "编译方式，代码按照Unicode   encoding! 
6.   Hope   it   has   some   help   to   you. 
********************/ 
        实际上只要下面两步骤: 
1.project下面选setting，然后选C/C++,在preprocessor   definitions中加入 
_UNICODE.(不需要去掉_MBCS) 
2.就是上面的4. 
      有了上面步骤,就说明你的工程已经是Unicode工程了.如果你是想把原来的程序 
改造成Unicode.编译后可能会发生错误,这就要求你改正成Unicode的正确形式..一 
般情况下对,字符串常量..你只要在前面加上_T( "字符串常量 "),就可以了.剩下的 
多为函数调用和字符转换的问题如strcpy-> lstrcpy和CString <-> Char   *之类的(可 
以看核心编程)..关于这个转换问题,我这里有一个出自CSDN精华区的网页(别人的 
总结),不知道现在里面有没有了？如果没有你想要,可以和我联系告知.我将尽力帮 
助.这里对_T宏说明一点,其实它在ANSI环境下也是通过的.而且就读成相应的环境 
形式.所以一般情况下,我觉得编程,应该都加上这个宏.免得将来麻烦:).还有一个 
要考虑的,Unicode因为都是双字节的,所以读取的时候要注意,特别是在写文件的时 
候.首先,如果你要让你的文件成为Unicode文件,一定要在文件头加上0xFFFE.还有 
假如你用的是CFile   读写文件也要注意.如果你创建的是以非文本读写的..你应该 
考虑下面的问题: 
        CFile   file; 
        CString   htm; 
        if(!file.Open(filename,CFile::modeCreate|CFile::modeWrite)) 
                  //Open   fail; 
        WORD   wFlag=0xFEFF; 
        file.Write(&wFlag,2);     //这是写Unicode文件标志头. 
        htm=_T( "alkdsj   flajdflajdlsfj ") 
        file.Write(htm,htm.GetLength()+1);//这是错误的.应该把后面的长度这样 
才能把htm内容完整的写到文件里面. 
                                                                            //你可以自己考虑具体问题出现在哪. 
:). 
        接着,我再说一下关于Rich   Edit控件的问题.因为是昨天刚遇到,所以今天就抽 
空帮别人解决了.他的问题是这样,在2000下,Unicode工程,RichEdit控件中输入小 
语种(日文,阿拉伯),无法用GetWindowText等函数获取.(获取的都是????).问题原 
因主要是因为: 
(1).Rich   Edit控件的版本太低,如RichEdit1.0(riched32.dll)就是不支持Unicod 
e的.所以必须换成RichEdit2.0或者3.0(riched20.dll)的.所以在你的程序InitIn 
stance()里面应该有下面代码: 
  HINSTANCE   richEdit=::LoadLibrary(_T( "riched20.dll ")); 
  if(!richEdit) 
    AfxMessageBox(_T( "Unable   to   load   RichEdit2.0 ")); 
  AfxInitRichEdit(); 
(2).修改你的资源*.rc.打开rc文件.(Open-> *.rc|Text方式|)..把原来的 "RICHED 
IT "改成 "RICHEDIT20W "就可以了.如下: 
        CONTROL                   " ",IDC_RICHEDIT1, "RICHEDIT20W ",WS_BORDER   | 
        这样,你利用GetWindowText,就可以得到你要的东西.也就是解决上面的问题。 

        这儿,我提供方法给那些没有用过RichEdit的人去构造这个控件. 
A.当你在对话框中有了Rich   Edit后..你编译时发现不能弹出对话框了.请在你的I 
nitInstance()开头加上.AfxInitRichEdit(); 
B.如果你初始化的话..用下面方式(在OnInitDialog里面加) 
  SetIcon(m_hIcon,   TRUE);       //   Set   big   icon 
  SetIcon(m_hIcon,   FALSE);     //   Set   small   icon 
  CHARFORMAT   cfDefault; 
  memset(&cfDefault,0,sizeof(cfDefault)); 
  cfDefault.cbSize=sizeof(cfDefault); 
  cfDefault.dwMask   =     CFM_FACE   |   CFM_SIZE   |CFM_CHARSET   |   CFM_SPACING; 
  cfDefault.yHeight   =   200; 
  cfDefault.bCharSet   =ARABIC_CHARSET;   //这是阿拉伯文的.这个好象不会影响控 
件.|我没研究清楚.不过是不可以用来让我们在非Unicode中获取文字的(或者没有 
上面解决方式). 
  strcpy(cfDefault.szFaceName, "Arial "); 
  m_RichEdit1.SetDefaultCharFormat(cfDefault);//设置模式 
c.如果要设置你的RichEdit响应事件消息(Envent)用: 
                CWnd*   pWnd=GetDlgItem(IDC_RICHEDIT1); 
  CRichEditCtrl*   pRich=(CRichEditCtrl*)(pWnd); 
  long   nEvn=pRich-> GetEventMask(); 
  nEvn   |=ENM_CHANGE;               //响应OnChangeEdit函数. 
  pRich-> SetEventMask(nEvn); 
        上面这些就是RichEdit入门.呵呵.. 
        我这里有一个我测试时写的一个源程序.(很简单）.可以用来输入多国语言(用 
的是RichEdit),并且获取显示.还有就是把他的Unicode代码显示出来.如果你要.. 
请和我联系.我给你们发过去.(尽力而为). 
        这里我还要提出一些关于显示多国语言里面我曾经思考的问题..作为参考留给 
大家..至于实现,我这个低级选手暂时只能望尘莫及了. 
(1):我当时曾考虑过如何在98下显示并获取的问题,当中用到了vb上面的一个Rich 
Text控件.那时侯发现,其实里面的文本可以用RTF格式取出来.但是RTF格式及其让 
人讨厌..所以当时是在没有办法的情况下(转悠了15天,急了),才考虑到准备把里面 
的内码值抠出来..呵呵. 
(2):仔细分析情况,(我个人认为)为什么会得到???,是因为内码对应不正确的问题 
..或者字符集不正确.例如:我们现在的操作环境对应的是GB2312码(里面有中文繁 
体字,以及中文日文字,我们虽然可以输入,但却被认成了中文的相应字符).当你把 
控制面版里区域选项下面的预设默认值设为中文简体时,它将被选入系统.以至于影 
响到你的vc程序.导致了无法获取的问题.关于这内码还要提一点..如果你是用繁体 
的,由于他没有简体和日文字体,所以无法正确显示.这里要说明一下..设置这个预 
设默认值,是为了构造环境,如果你预设是日文,然后你的程序连接也用的是日文资 
源dll.那么你的程序即便是在中文Win平台下,用vc，在这时候也可以获得对日文的 
支持显示和处理功能.(其实就是相单于构造了日文操作平台). 
(3):这个和支持多国语资源没有关系..如果你想在你资源里面多加日文资源,那只 
能保证你可以用   日文windows显示那些文字.但却不能共同显示中文,日文.我看到 
了 < <用VC5_0实现软件中的多种语言支持> > 文章..完全和这个是不一样的事情.而且 
它和我们常遇到的多国语言下载组件支持也没有任何关系. 
(4):我的一个同事由于研究给软件安装输入法时,发现可以利用DHTML控件来支持呼 
叫和显示,无论是98还是2000.都可以,只不过里面内容都为网页,所以必须自己抠出 
来(我想不会很难). 
        好了,就写到这里,算是我这一段时间的总结,呵呵..如果你头晕了,千万要记得 
,我的交友申请,欢迎和我联系,一起研究,另外本人可能要转入多媒体了,也希望多 
交一些搞多煤体的朋友. 
我的QQ:8802311.Email:xie_yong_ping@163.com.还有,很希望和那些想搞清楚在9 
8下来支持这些显示的人,互相探讨.你们都可以在下面留下联系方式. 
        如果你要查询资料建议你到google里面寻找..最好的方式就是看看MSDN..然后 
对照..呵呵。.以后遇到这方面的麻烦。。大家可以找我了。。。 

我已经将我的东西打包了。里面包括一些Unicode的文章、数据转换方法（csdn精 
华网页）、RichEdit控件的相关文章。。以及还有我上面说的源程序打包了。。你 
可以索要。（留下联系方式）。 
        这里要对上面的DHTML做个补充说明：为什么用网页可以支持显示多国语言。 
这主要是因为只要是IE5.0以上的版本。。微软在做IE控件的时候，就让它支持了 
Unicode字符（里面源码多是&#x0032或&#12345;)，这就使得我们在98下利用DHTML 
做控件来显示多国语言成为了可能。其实Office也是和Ie一样支持多国语言的显示 
。（这个大家都应该是清楚的）。还有Outlook.Word. 

实在对不住..刚看了一遍,下面有一处地方没写完整 
        file.Write(&wFlag,2);     //这是写Unicode文件标志头. 
        htm=_T( "alkdsj   flajdflajdlsfj ") 
        file.Write(htm,htm.GetLength()+1);//这是错误的.应该把后面的长度       乘 
以2           这样才能把htm内容完整的写到文件里面. 

/******************************************/ 
还有一点要注意。。那源代码的可执行文件不能在98下运行。。而且你还要保证你 
机子里面有最新的riched20.dll.(我当时没有把他付送到发给你们的文件里面。。 
 
 
