1、在APP.cpp中加入：

#include "SAStatusLog.h"



/////////////////////////////////////////////////////////////////////////////
// The one and only CCStatusLogDemoApp object

CCStatusLogDemoApp theApp;

// the one and only CStatusLog object
CSAStatusLog g_statusLog;


2、在APP.h中加入
#include "SAStatusLog.h"

extern CSAStatusLog g_statusLog;




vc全局变量一般这样定义：  

2010-09-10 21:40:48|  分类： 个人日记 |字号 订阅
全局变量一般这样定义：
1。在一类的.cpp中定义 int myInt;
然后再在要用到的地方的.cpp里extern int myInt；这样就可以用了。
2。在stdafx.cpp中加入:
int myInt;
然后在stdafx.h中加入:
extern int myInt
这样定义以后无论在什么文件中都是可见的.
3。比较规范的是，先定义一个Glbs.h，把所有的全局变量原始定义放进去。然后定义一个Externs.h，
把你先前定义在Glbs.h中的变量都加上extern。注意：如果你在Glbs.h中设置了初值，那么在Externs.h
中就不要加值了。然后调用时，第一次调用的＃i nclude <Glbs.h>，以后调用的＃i nclude 
<Externs.h>
另：
问：如何在VC++中使用全局变量，以使文档中的所有类都能访问。 
  答：把该变量放到该应用程序类的头文件中的attribute处。然后，在程序的任何地方，你都可以用
下面的方法来访问该变量： 
  CMyApp *app=（CMyApp*）AfxGet-App（）； 
  app->MyGlobalVariable=… 
  用这个方法，不但可以定义全局变量，也可以定义全局对象。 
  例如： 
  MyClass MyObject； 
  CMyApp*app=（CMyApp*）AfxGet-App（）； 
  app->MyObject.MyFunction（）； 
VC中使用全局变量的2种办法及防错措施  
1. 对于全局变量存在和函数一样的问题，为了在其他CPP文件中能够访问这些变量，必须在主文件的H文
件中加上extern声明，格式如下： 
extern varibletype var; （声明）
在主文件的CPP文件中定义 
varibletype var; （定义）
例子: 
AppWizard建立一个Test工程 
那么在Test.h中声明extern CString cs; 
在Test.app定义CString cs; 
   如果要定义整个工程的全局变量，在任何一个CPP文件中进行定义，然后在需要引用这个变量的文件
中进行声明。如全局变量很多可以选择使用定义全局变量的。h文件，在需要的地方直接include头文件
即可，不需要写那么多extern了。
2.应用程序类的主头文件处定义变量varibletype var，然后，在程序的任何地方，都可以用下面的方法
来访问该变量：  
  CClassApp *app=(CClassApp*)AfxGetApp();  
  app->var= 
 类似的，以上方法也可以定义全局对象 
例子: 
AppWizard建立一个Test工程 
那么在Test.h中声明 CString cs; 
使用的时候CTestApp *app=(CTestApp*)AfxGetApp();  
    app->cs="Global" 
 
防错措施： 
若定义的函数和全局变量在多个文件包含且造成嵌套或多次调用的话，这样将导致这个头文件每被包含
依次，函数或变量就被重新定义一次，在链接编译时会导致重定义错误。为此需要使用一种被称为Guard 
macro的技术来保证不出错。在一个头文件开头加上  
#ifndef   _MACRO_1_ 
#define   _MACRO_1_ 
在文件末尾增加  
#endif 
 另外转一下一位朋友写的在MFC中定义全局变量
在MFC下如何定义全局变量和全局函数VC++
用MFC制作的工程由很多文件构成，它不能象一般C++程序那样随意在类外定义全局变量，在这里要想定
义能被工程内多个文件共享的全局变量和函数必须用一些特殊方法才行。实际上有多种方法可以实现，
这里只介绍两种方法。
一、在应用程序类中定义
用MFC生成的工程中都有一个名为CxxxApp的类，它派生于CWinApp类。这个类主要进行程序的初始化，生
成文档、视图对象等工作。我们可以把需要全局访问的变量和函数定义为这个类的成员变量和成员函数
，就可以实现全局访问了。
从严格意义上讲，这种变量和函数并不是全局的，因为它仍然只是类中的成员，只是由于我们很容易获
得CxxxApp类的指针，所以我们可以在文档、视 图、对话框以及各种自定义类中访问到它们，达到与全
局变量类似的效果。访问时用函数“AfxGetApp()”获得CxxxApp类的指针，用 “AfxGetApp()->成员”
访问变量或函数。
例：
Test.h：（应用程序类头文件）
class CTestApp : public CWinApp
{
public:
int x; //全局变量
int f(int y); //全局函数
…………
};
Test.cpp：（应用程序类程序文件）
int CTestApp::f(int y) //全局函数定义
{
y++;
return y;
}
定义在CTestApp类中的变量和函数可以在其它类中被访问。比如在视图的某函数中要访问变量x和函数f
()：
void CTestView::xyz()
{
CTestApp *app = (CTestApp *)AfxGetApp(); //生成指向应用程序类的指针
app->x = 0; //访问变量x
int z = app->f(1); //访问函数f()
…………
}
这样，变量x和函数f()可以视作为全局的。
用这种方法实现的全局变量和全局函数虽比较简单，但也有缺点，一是访问不太方便，每次都需要获取
应用程序类的指针；再就是把一些与应用程序类本身无关的变量和函数放在里面，使这个类看上去怪怪
的，破坏了类的封装。
二、用静态变量和静态函数实现
很喜欢API函数的那种调用方法，不论在哪个类中只要用“::API函数”就可以调用了。合理利用静态类
型(static)可以实现与此相似的全局变量和全局函数。
静态变量和静态函数有如下性质：
若在一个类中用关键字static声明数据成员，则这个数据成员就只存在一个拷贝，无论该类创建了多少
个实例，它始终只存在一个，即使该类的实例一个也没创建，它也存在。
若在一个类中用关键字static声明函数，该函数可以用“类名::函数名”方式访问，无需引用该类的实
例，甚至这个类的实例可以不存在。
利用这个性质实现的全局变量和函数使用起来很方便。
值得注意的是，全局变量和全局函数最好集中封装，不要在文档、视图等类内部定义，这样用起来才有
全局的感觉。
例：
1、添加一个没有基类的新类，设类名起为CPublic，姑且称之为公用类
单击“Insert”菜单下的“New Class”命令，选择“Class type”为“Generic Class”，在“Name”
栏中填入类名“CPublic”，单击“OK”，则新类建立完毕。
2、包含公用类的头文件，使各个类都能访问它
CPublic的头文件应包含在应用程序类的头文件中，这样在其它类中引用CPublic类时就不需要再包含了
。
Test.h：（应用程序类头文件）
#include "Public.h" //包含公用类头文件
class CTestApp : public CWinApp
{
…………
};
3、在公用类中定义全局变量和全局函数，均使用static修饰，静态变量还必须在类外定义和初始化
Public.h：（公用类头文件）
class CPublic
{
public:
CPublic();
virtual ~CPublic();
public:
static int x; //全局变量
static int time; //全局变量
static int f(int y); //全局函数
…………
}
在公用类中对静态变量进行初始化和定义函数体：
Public.cpp：（公用类程序文件）
int CPublic::x = 0; //初始化全局变量
int CPublic::time; //定义全局变量
CPublic::CPublic()
{
}
CPublic::~CPublic()
{
}
int CPublic::f(int y) //全局函数，这里不要再加static
{
y++;
return y;
}
4、全局量的使用
使用变量：CPublic::变量名
使用函数：CPublic::函数()
如在视图的某函数中访问变量x和函数f()：
void CTestView::xyz()
{
CPublic::x = 0; //访问变量x
CPublic::time = CPublic::f(1); //访问函数f()
…………
}
在其它类中访问x、time和f()的方法与此相同。
5、几点注意：
① 由于静态量可独立于类存在，不需要生成CPublic类的实例。
② 静态数据成员的定义和初始化必须在类外进行，如例中x的初始化；变量time虽然没有初始化，但也
必须在类外进行定义。由于没有生成CPublic类的实例，所以它的构造函数和析构函数都不会被执行，在
里面做什么工作都没有什么意义。
③ 如果静态函数需要访问CPublic类内的变量，这些变量也必须为静态的。因为非静态量在不生成实例
时都不会存在。 如：
class CPublic
{
public:
int x; //内部变量
static int f(int y) //全局函数
{
x++;
return x;
};
…………
};
这里x虽为类内成员，但如果不生成CPublic类的实例，就会出现函数f()存在，而变量x不存在的问题。
总之，用没有实例的类管理全局量是一个不错的选择，它具有集中管理，使用方便的好处。当然，除非
特别必要，全局量还是少用为好，一个好的编程者决不会随意滥用全局量的，一个封装做得不好的程序
，在修改维护时会让你吃足苦头。
//////////////////////////
VC中如何定义全局变量 
答: 
  放在任意文件里  
  在其他文件里用 extern 声明  
  或者在App类中声明一个公有成员  
  在程序其它类中可以用  
  AfxGetApp() 得到一个App对象指针，将它转化成你的App类指针  
  然后就可以通过指针->变量使用了  
  还有就是在  
  MFC中定义只有静态成员的类 到时候直接应用就可以了，还不 
  破坏结构 
答:也可以新建一个.h的头文件，专门用来放全局变量。哪里要用到它，就Include一下 
全局变量一般这样定义： 
1。在一类的.cpp中定义 int myInt; 
然后再在要用到的地方的.cpp里extern int myInt；这样就可以用了。 
2。在stdafx.cpp中加入: 
int myInt; 
然后在stdafx.h中加入: 
extern int myInt 
这样定义以后无论在什么文件中都是可见的. 
3。比较规范的是，先定义一个Glbs.h，把所有的全局变量原始定义放进去。然后定义一个Externs.h，
把你先前定义在Glbs.h中的变量都加上extern。注意：如果你在Glbs.h中设置了初值，那么在Externs.h
中就不要加值了。然后调用时，第一次调用的＃i nclude <Glbs.h>，以后调用的＃i nclude 
<Externs.h> 
另： 
问：如何在VC++中使用全局变量，以使文档中的所有类都能访问。  
  答：把该变量放到该应用程序类的头文件中的attribute处。然后，在程序的任何地方，你都可以用
下面的方法来访问该变量：  
  CMyApp *app=（CMyApp*）AfxGet-App（）；  
  app->MyGlobalVariable=…  
  用这个方法，不但可以定义全局变量，也可以定义全局对象。  
  例如：  
  MyClass MyObject；  
  CMyApp*app=（CMyApp*）AfxGet-App（）；  
  app->MyObject.MyFunction（）；  
VC中使用全局变量的2种办法及防错措施  
1. 对于全局变量存在和函数一样的问题，为了在其他CPP文件中能够访问这些变量，必须在主文件的H文
件中加上extern声明，格式如下：  
extern varibletype var; （声明） 
在主文件的CPP文件中定义  
varibletype var; （定义） 
例子:  
AppWizard建立一个Test工程  
那么在Test.h中声明extern CString cs;  
在Test.app定义CString cs;  
 如果要定义整个工程的全局变量，在任何一个CPP文件中进行定义，然后在需要引用这个变量的文件中
进行声明。如全局变量很多可以选择使用定义全局变量的。h文件，在需要的地方直接include头文件即
可，不需要写那么多extern了。 
2.应用程序类的主头文件处定义变量varibletype var，然后，在程序的任何地方，都可以用下面的方法
来访问该变量：  
  CClassApp *app=(CClassApp*)AfxGetApp();  
  app->var=  
 类似的，以上方法也可以定义全局对象  
例子:  
AppWizard建立一个Test工程  
那么在Test.h中声明 CString cs;  
使用的时候CTestApp *app=(CTestApp*)AfxGetApp();  
 app->cs="Global"  
  
防错措施：  
若定义的函数和全局变量在多个文件包含且造成嵌套或多次调用的话，这样将导致这个头文件每被包含
依次，函数或变量就被重新定义一次，在链接编译时会导致重定义错误。为此需要使用一种被称为Guard 
macro的技术来保证不出错。在一个头文件开头加上  
#ifndef _MACRO_1_  
#define _MACRO_1_  
在文件末尾增加  
#endif  
 另外转一下一位朋友写的在MFC中定义全局变量 
在MFC下如何定义全局变量和全局函数VC++ 
用MFC制作的工程由很多文件构成，它不能象一般C++程序那样随意在类外定义全局变量，在这里要想定
义能被工程内多个文件共享的全局变量和函数必须用一些特殊方法才行。实际上有多种方法可以实现，
这里只介绍两种方法。 
一、在应用程序类中定义 
用MFC生成的工程中都有一个名为CxxxApp的类，它派生于CWinApp类。这个类主要进行程序的初始化，生
成文档、视图对象等工作。我们可以把需要全局访问的变量和函数定义为这个类的成员变量和成员函数
，就可以实现全局访问了。 
从严格意义上讲，这种变量和函数并不是全局的，因为它仍然只是类中的成员，只是由于我们很容易获
得CxxxApp类的指针，所以我们可以在文档、视 图、对话框以及各种自定义类中访问到它们，达到与全
局变量类似的效果。访问时用函数“AfxGetApp()”获得CxxxApp类的指针，用 “AfxGetApp()->成员”
访问变量或函数。 
例： 
Test.h：（应用程序类头文件） 
class CTestApp : public CWinApp 
{ 
public: 
int x; //全局变量 
int f(int y); //全局函数 
………… 
}; 
Test.cpp：（应用程序类程序文件） 
int CTestApp::f(int y) //全局函数定义 
{ 
y++; 
return y; 
} 
定义在CTestApp类中的变量和函数可以在其它类中被访问。比如在视图的某函数中要访问变量x和函数f
()： 
void CTestView::xyz() 
{ 
CTestApp *app = (CTestApp *)AfxGetApp(); //生成指向应用程序类的指针 
app->x = 0; //访问变量x 
int z = app->f(1); //访问函数f() 
………… 
} 
这样，变量x和函数f()可以视作为全局的。 
用这种方法实现的全局变量和全局函数虽比较简单，但也有缺点，一是访问不太方便，每次都需要获取
应用程序类的指针；再就是把一些与应用程序类本身无关的变量和函数放在里面，使这个类看上去怪怪
的，破坏了类的封装。 
二、用静态变量和静态函数实现 
很喜欢API函数的那种调用方法，不论在哪个类中只要用“::API函数”就可以调用了。合理利用静态类
型(static)可以实现与此相似的全局变量和全局函数。 
静态变量和静态函数有如下性质： 
若在一个类中用关键字static声明数据成员，则这个数据成员就只存在一个拷贝，无论该类创建了多少
个实例，它始终只存在一个，即使该类的实例一个也没创建，它也存在。 
若在一个类中用关键字static声明函数，该函数可以用“类名::函数名”方式访问，无需引用该类的实
例，甚至这个类的实例可以不存在。 
利用这个性质实现的全局变量和函数使用起来很方便。 
值得注意的是，全局变量和全局函数最好集中封装，不要在文档、视图等类内部定义，这样用起来才有
全局的感觉。 
例： 
1、添加一个没有基类的新类，设类名起为CPublic，姑且称之为公用类 
单击“Insert”菜单下的“New Class”命令，选择“Class type”为“Generic Class”，在“Name”
栏中填入类名“CPublic”，单击“OK”，则新类建立完毕。 
2、包含公用类的头文件，使各个类都能访问它 
CPublic的头文件应包含在应用程序类的头文件中，这样在其它类中引用CPublic类时就不需要再包含了
。 
Test.h：（应用程序类头文件） 
#include "Public.h" //包含公用类头文件 
class CTestApp : public CWinApp 
{ 
………… 
}; 
3、在公用类中定义全局变量和全局函数，均使用static修饰，静态变量还必须在类外定义和初始化 
Public.h：（公用类头文件） 
class CPublic 
{ 
public: 
CPublic(); 
virtual ~CPublic(); 
public: 
static int x; //全局变量 
static int time; //全局变量 
static int f(int y); //全局函数 
………… 
} 
在公用类中对静态变量进行初始化和定义函数体： 
Public.cpp：（公用类程序文件） 
int CPublic::x = 0; //初始化全局变量 
int CPublic::time; //定义全局变量 
CPublic::CPublic() 
{ 
} 
CPublic::~CPublic() 
{ 
} 
int CPublic::f(int y) //全局函数，这里不要再加static 
{ 
y++; 
return y; 
} 
4、全局量的使用 
使用变量：CPublic::变量名 
使用函数：CPublic::函数() 
如在视图的某函数中访问变量x和函数f()： 
void CTestView::xyz() 
{ 
CPublic::x = 0; //访问变量x 
CPublic::time = CPublic::f(1); //访问函数f() 
………… 
} 
在其它类中访问x、time和f()的方法与此相同。 
5、几点注意： 
① 由于静态量可独立于类存在，不需要生成CPublic类的实例。 
② 静态数据成员的定义和初始化必须在类外进行，如例中x的初始化；变量time虽然没有初始化，但也
必须在类外进行定义。由于没有生成CPublic类的实例，所以它的构造函数和析构函数都不会被执行，在
里面做什么工作都没有什么意义。 
③ 如果静态函数需要访问CPublic类内的变量，这些变量也必须为静态的。因为非静态量在不生成实例
时都不会存在。 如： 
class CPublic 
{ 
public: 
int x; //内部变量 
static int f(int y) //全局函数 
{ 
x++; 
return x; 
}; 
………… 
}; 
这里x虽为类内成员，但如果不生成CPublic类的实例，就会出现函数f()存在，而变量x不存在的问题。 
总之，用没有实例的类管理全局量是一个不错的选择，它具有集中管理，使用方便的好处。当然，除非
特别必要，全局量还是少用为好，一个好的编程者决不会随意滥用全局量的，一个封装做得不好的程序
，在修改维护时会让你吃足苦头。
/////////////////////
VC++中关于全局量的定义有两种比较好的方法。
第一种
新建一个Generic Class类，如CGlobals。会产生一个Globals.h头文件和一个Globals.cpp文件，在
ClassView中会产生一个 CGlobals类。由于主要目的是利用产生的.h和.cpp文件，所以，将两个文件中
关于类声明和定义的部分都删除或注释掉，但保留其他语句，此时 ClassView中的CGlobals类消失。
对于全局结构体定义，首先在Globals.h头文件中构造结构体，用typedef方式，如：
typedef struct tagLineType
{
int ID;
CString Type;
double r0;
double x0;
}LINETYPE;    // 名称可随便
而后，在Globals.cpp文件中：
LINETYPE *lineType = NULL；    // 注意，在这里初始化
而后，回到Globals.h头文件：
extern LINETYPE *lineType；    // 注意，这里不能再初始化
对于普通全局变量（如int型）定义，首先在Globals.cpp中：
int iTime = -1；    // 注意，在这里初始化
而后，在Globals.h头文件中：
extern int iTime；    // 注意，这里不能再初始化
对于全局常量定义，首先再Globals.cpp中：
const int UB = 10；
而后在Globals.h头文件中：
extern const int UB；
以上是第一种方法，是我自己总结试验成功的。用该方法定义完毕后，无论哪个.cpp文件要用到全局变
量，只需在该.cpp文件中#include "Globals.h"即可，而无需再在.cpp文件中extern，所有全局变量、
全局常量、全局结构体都是可见的。该方法的总体思路是，象 theApp那样，在.cpp中定义，在.h头文件
中extern声明。另外要注意，变量的初始化一定在Globals.cpp文件中，而不能在 Globals.h头文件中。
第二种
新建一个.h头文件，如Globals.h，将所有要定义的全局变量在该头文件种定义。而后新建另一个头文件
，如GlobalsExt.h，将所有在 Globals.h中定义的全局变量extern声明，同样，初始化工作要在
Globals.h中进行。定义完毕后，在第一次要使用全局变量的.cpp文件中，#include "Globals.h"头文件
，在以后要使用全局变量的.cpp文件中，#include "GlobalsExt.h"头文件。
该方法据说是一种比较规范的定义方法，网上有相关文章。但我没有试成功过，总是提示有重复定义。
而且，对于该方法下如何定义结构体亦没有明确说明。哪位高手可以指点一下。


