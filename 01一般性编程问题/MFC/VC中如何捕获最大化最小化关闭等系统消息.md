VC中如何捕获最大化最小化关闭等系统消息

http://www.a3gs.com/BookViews.asp?InfoID=2730&classID=819

编号：A3GS_TV20091224001


描述：


讲述在VC中如何捕获最大化、最小化、关闭、大小变化等系统消息，同时给出了获取客户区大小的方法。


参考：


《VC如何截获Windows 关机消息》《VC中实现自定义消息》《VC处理键盘消息》《VC6对话框隐藏的几种方法》


例子：


请参下载本文例子工程


技术实现：


实现捕获最大化、最小化、关闭、大小变化等系统消息主要有如下两种方法：


1． 重写PreTranslateMessage函数

BOOL CTestWinMinMaxMsgDlg::PreTranslateMessage(MSG* pMsg)

{

     // TODO: Add your specialized code here and/or call the base class

     if ( pMsg->message == WM_SYSCOMMAND )

     {

         if ( ( pMsg->wParam & 0xFFF0) == SC_MINIMIZE )

         {

              AfxMessageBox( "捕获到窗口最小化消息" );

         }else if ( ( pMsg->wParam & 0xFFF0) == SC_MAXIMIZE )

         {

              AfxMessageBox( "捕获到窗口最大化消息" );

         }else if ( ( pMsg->wParam & 0xFFF0) == SC_CLOSE )

         {

              AfxMessageBox( "捕获到窗口关闭消息" );

         }

     }

     return CDialog::PreTranslateMessage(pMsg);

}


注：此方法无法捕获对话框程序程序中的系统按钮消息


2． 响应WM_SIZE消息(OnSize函数)

void CTestWinMinMaxMsgDlg::OnSize(UINT nType, int cx, int cy)

{

     CDialog::OnSize(nType, cx, cy);


     // TODO: Add your message handler code here

     int nWidth;

     int nHeight;

     //   width   of   client   area

     nWidth   =   LOWORD(lParam);

     //   height   of   client   area

     nHeight =   HIWORD(lParam);


     switch( nType )

     {

     case SIZE_RESTORED:

         AfxMessageBox( "捕获到窗口还原消息" );

         break;

     case SIZE_MINIMIZED:

         AfxMessageBox( "捕获到窗口最小化消息" );

         break;

     case SIZE_MAXIMIZED:

         AfxMessageBox( "捕获到窗口最大化消息" );

         break;

     }

}

注：此方法不但可以捕获对话框程序程序中的系统按钮消息，同时还可以获取客户区高度与宽度。


附加说明：


你也可以用IsZoomed函数来判断窗口是否最大化了。