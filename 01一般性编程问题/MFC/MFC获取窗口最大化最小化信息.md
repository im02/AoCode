
### 源码名称：MFC获取窗口最大化最小化信息 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
MFC获取窗口最大化最小化信息
```
#### 下载：无
#### 使用方法：


``` c++
方法1:在WM_SYSCOMMAND的响应函数中处理：

afx_msg void OnSysCommand( UINT nID, LPARAM lParam );

判断第一个参数：
SC_MAXIMIZE (or SC_ZOOM)   Maximize the CWnd object.
SC_MINIMIZE (or SC_ICON)   Minimize the CWnd object

方法2：在虚函数PreTranslateMessage中判断消息是否为WM_SYSCOMMAND消息，如果是，则判断附加参数是否为最大化或者最小化。


if (pMsg->message==WM_SYSCOMMAND)
{
   if (pMsg->lParam==SC_MINIMIZE)
   {
      //TODO:在此添加你的处理代码
   }
   else if (pMsg->lParam==SC_MAXIMIZE)
   {
     //TODO:在此添加你的处理代码
   }
}
```
