
### 源码名称：CTime与CString转换 
#### 修改时间：20180212
#### 语言：VC++/MFC
#### 使用项目：[Cool02.com](http://w.cool02.com)
#### 实例链接：无
#### 源码说明：
```
CTime与CString转换
```
#### 下载：无
#### 使用方法：


``` c++
1.unsigned long->CTime
  unsigned long nTime;   //nTime类型为unsigned long
  CTime objTime(nTime);   //CTime类型变量直接获取时间值
  string strTime;    //格式化string变量
  strTime.Format("d-d-d d:d:d", objTime.GetYear(), objTime.GetMonth(), objTime.GetDay(),
  objTime.GetHour(), objTime.GetMinute(), objTime.GetSecond());
  printf("%s\n", strTime);


2.CTime -> unsigned long
  CTime objTime;     //定义CTime类型变量
  m_DateTimeCtrlTime.GetTime(objTime);  //在MFC界面获取时间值（Date Time Picker）
  unsigned long nTime = objTime.GetTime(); //unsigned long类型变量获取时间长度

==================================================================

    CString strDate;
    COleDateTime ole_time;
    CTime c_time;
3.COleDateTime->CTime
  SYSTEMTIME sys_time;
  ole_time.GetAsSystemtime(sys_time);
  c_time = CTime(sys_time);
4.CTime->COleDateTime
  SYSTEMTIME sys_time;
  c_time.GetAsSystemTime(sys_time);
  ole_time = COleDateTime(sys_time);
5.CString->COleDateTime
  strDate = "2009-4-25 12:30:29";
  ole_time.ParseDateTime(strDate);
6.COleDateTime->CString
  strDate = ole_time.Format("%Y-%m-%d %H:%M:%S");
7.CTime->CString

  strDate = c_time.Format("%Y-%m-%d %H:%M:%S");

================================================================

8.unsigned long->time_t
  unsigned long nTime;   //nTime初始类型为unsigned long
  ...     //nTime赋值
  time_t tTime = (time_t)nTime;  //将nTime（32位）强制转换为time_t类型，或者INT64类型（64位）
  struct tm *tmTime;   //定义tm类型指针
  tmTime = localtime(&tTime);  //获取时间
  printf("%s\n", asctime(tmTime));  //将时间表示为英文语句形式，如：Mon Dec 28 12:33:50 1998

9. time_t -> unsigned long
  CString strTime;   //CString类型
  ...     //在MFC界面获取时间值
  struct tm stTime;   //定义tm类型变量
  sscanf((LPSTR)(LPCTSTR)strTime, "d-d-d d:d:d",
  	&stTime.tm_year, &stTime.tm_mon, &stTime.tm_mday, &stTime.tm_hour,
  &stTime.tm_min, &stTime.tm_sec); //强制转换为char*，再使tm变量获取时间值
  stTime.tm_year = stTime.tm_year - 1900;
  stTime.tm_mon = stTime.tm_mon - 1; //转换相对1900.01的时间长度
  unsigned long nTime = (long)_mkgmtime(&stTime);  //转换成了unsigned long类型



C++中，CTime 与  CString转换
CTime m_StartTime1 = CTime::GetCurrentTime();
CString csStartTime = m_StartTime1.Format( "%Y%m%d%H%M%S" );

 使用sprintf及sscanf函数进行CString与CTime之间的转换
 

一.将CString转为CTime的几种方法

CString   timestr   =   "2000年04月05日";   
  int   a,b,c   ;   
  sscanf(timestr.GetBuffer(timestr.GetLength()),"%d年%d月%d日",&a,&b,&c);   
  CTime   time(a,b,c,0,0,0);     


--------or - ---------------------

 CString   s("2001-8-29   19:06:23");   
  int   nYear,   nMonth,   nDate,   nHour,   nMin,   nSec;   
  sscanf(s,   "%d-%d-%d   %d:%d:%d",   &nYear,   &nMonth,   &nDate,   &nHour,   &nMin,   &nSec);   
  CTime   t(nYear,   nMonth,   nDate,   nHour,   nMin,   nSec);

---- or ------------------------
CString   timestr   =   "2000年04月05日";   
  int   year,month,day;   
  BYTE   tt[5];   
  //get   year   
  memset(tt,   0,   sizeof(tt));   
  tt[0]   =   timestr[0];   
  tt[1]   =   timestr[1];   
  tt[2]   =   timestr[2];   
  tt[3]   =   timestr[3];   
  year=   atoi((char   *)tt);   
    
  //get   month   
  memset(tt,   0,   sizeof(tt));   
  tt[0]   =   timestr[6];   
  tt[1]   =   timestr[7];   
  month   =   atoi((char   *)tt);   
    
  //get   day   
  memset(tt,   0,   sizeof(tt));   
  tt[0]   =   timestr[10];   
  tt[1]   =   timestr[11];   
    
  CTime   time(year,month,day,0,0,0);

从上面来看,很明显使用sscanf()函数的优势.

 

二.将CTIme转换为CString的方法:

CTime  tmSCan = CTime::GetCurrentTime();

CString szTime = tmScan.Format("'%Y-%m-%d %H:%M:%S'");

这样得到的日期时间字符串就是以"2006-11-27 23:30:59"的格式.这是不是很方便呢?

 //取得CTime中的日期
 CString cstrDate = tmScan.Format("%Y-%m-%d");

 //取得CTime中的时间
 CString cstrTime = tmScan.Format("%H:%M-%S");

          sprintf还有个不错的表妹：strftime，专门用于格式化时间字符串的，用法跟她表哥很像，也是一大堆格式控制符，只是毕竟小姑娘家心细，她还要调用者指定缓冲区的最大长度，可能是为了在出现问题时可以推卸责任吧。这里举个例子：

 

 更多更好的sprintf()函数说明参考:《spirntf，你知道多少？》

http://blog.csdn.net/steedhorse/archive/2005/03/25/330206.aspx

 

 

time_t t = time(0);




      //产生"YYYY-MM-DD hh:mm:ss"格式的字符串。

 




char s[32];




strftime(s, sizeof(s), "%Y-%m-%d %H:%M:%S", localtime(&t));




sprintf在MFC中也能找到他的知音：CString::Format，strftime在MFC中自然也有她的同道：CTime::Format，这一对由于从面向对象哪里得到了赞助，用以写出的代码更觉优雅。





```
